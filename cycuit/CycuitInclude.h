#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <Spinster/Spinster.h>
#include <NVX/NVX.h>
#include <ARKE/ARKE.h>

#include <NVX/Spatial.h>

namespace Cycuit {
    using namespace nvx;
    using namespace arke;
};
