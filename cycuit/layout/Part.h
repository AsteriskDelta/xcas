#ifndef CYCUIT_PART_H
#define CYCUIT_PART_H
#include "CycuitInclude.h"
#include "Subpart.h"
#include <vector>

namespace Cycuit {
    class Part : public Subpart {
    public:
        Part();
        virtual ~Part();

        void addPart(Subpart* part);

        inline virtual bool trivial() const override {
            return false;
        }
        inline virtual Part* part() override {
            return this;
        }
    protected:
        std::vector<Subpart*> subparts;
    };
}

#endif
