#ifndef CYCUIT_PIN_H
#define CYCUIT_PIN_H
#include "CycuitInclude.h"
#include "Drawable.h"

namespace Cycuit {
    class Pin : public nvx::transform2d_t, public Drawable {
    public:
        Pin();
        virtual ~Pin();

        nvx::geni_t diameter;
    protected:

    };
}

#endif
