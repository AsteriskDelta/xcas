#ifndef CYCUIT_PAD_H
#define CYCUIT_PAD_H
#include "CycuitInclude.h"
#include "Drawable.h"

namespace Cycuit {
    class Pad : public nvx::transform2d_t, public Drawable {
    public:
        Pad();
        virtual ~Pad();

        nvx::geni_t radii[2];
    protected:

    };
}

#endif
