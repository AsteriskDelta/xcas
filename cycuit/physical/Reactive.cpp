#include "Reactive.h"

namespace nvx {
    template class SphereMap<Cycuit::Phyector, Cycuit::AttributeSet>;
}

namespace Cycuit {
    Reactive::Reactive() {

    }

    Reactive::~Reactive() {

    }

    AttributeSet Reactive::evaluate(const Phyector& vec) const {
        return sphere.eval(vec);
    }
};
