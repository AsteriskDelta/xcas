#ifndef CYCUIT_REACTIVE_H
#define CYCUIT_REACTIVE_H
#include "../CycuitInclude.h"
#include "Phyector.h"
#include "AttributeSet.h"
#include <NVX/SphereMap.h>

namespace Cycuit {
    class Reactive {
    public:
        Reactive();
        virtual ~Reactive();

        /*struct CtrlVec {
            Phyector vector;
            AttributeSet set;
        };*/

        nvx::SphereMap<Phyector, AttributeSet> sphere;

        AttributeSet evaluate(const Phyector& vec) const;
    };
}

#endif
