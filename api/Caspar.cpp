#include "Caspar.h"
//#include "Patterns.h"

namespace Caspar {
    VM* CreateVM(VM *const parent, bool forceEmpty) {
        _unused(forceEmpty);
        VM* ret = new VM(parent);
        /*if(parent == nullptr && !forceEmpty) {
            if(!Patterns::Loaded()) Patterns::LoadAll();
            
            //Add all patterns to the "root" VM, by default
            ret->addPatterns(Patterns::All);
        }*/
        
        return ret;
    }
};
