#ifndef CASPAR_API_INC_H
#define CASPAR_API_INC_H
#include "iCaspar.h"
#include "ParserStream.h"
#include "XNode.h"
#include "VM.h"

namespace Caspar {
    VM* CreateVM(VM *const parent = nullptr, bool forceEmpty = false);
}

#endif
