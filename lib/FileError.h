#include <string>

namespace Caspar {
    
    void FileError(const std::string& msg, const std::string& path, const unsigned int lineNumber, const std::string& line);
};
