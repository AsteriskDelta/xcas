#ifndef CYCLIC_VECTOR_H
#define CYCLIC_VECTOR_H
#include "GenIterator.h"
#include "iCaspar.h"

template<typename V>
class CyclicVector {
public:
    typedef CyclicVector<V> Self;
    typedef V Value;
    typedef int Idt;
    typedef unsigned short Idx;
    struct iterator;
    
    //CyclicVector();
    explicit CyclicVector(Idt sz);
    ~CyclicVector();
    
    const Value* get(const Idt id) const;
    const Value* rawGet(const Idx& absID) const;
    _mutate(get,(const Idt id),(id));
    _mutate(rawGet,(const Idt id),(id));
    
    /*inline Value& front() const {
        return this->get(0);
    }
    inline Value& back() const {
        return this->get(-1);
    }
    _mutate(front,(),());
    _mutate(back,(),());
    */
    iterator push(const Value& val);
    iterator push(const Value *const ptr, const unsigned int count);
    Idt pop();//Pops from leading end
    //Idt consume(Idt count = 1);//Takes from trailing end, up to leading end
    
    Idt size() const;
    Idt maxSize() const;
    inline Idt available() const {
        return this->maxSize() - this->size();
    }
    
    void setMaxSize(const Idx& sz);
    void setCycleSize(const Idx& len);
    
    void clear();
    
    inline iterator nextToRecycle() {
        if(!this->saturated()) return this->invalid();
        else return this->end();
    }
    
    inline bool saturated() const {
        return edge.fill >= edge.max;
    }
    
    bool cycled() const;
    bool recycled() const;
    
    iterator begin() const;
    iterator end() const;
    iterator invalid() const;
    iterator iter(const Idt idt) const;
//protected:
    std::vector<Value> base;
    struct {
        Idx trailing = 0, leading = 0;
        Idx fill = 0, max = 0;
        Idx cycleCnt = 0;
    } edge;
    
    Idx cycleLength;
    
    inline Idx resolveIdt(const Idt idt) const;
public:
    
    inline bool empty() const {
        return this->size() == 0;
    }
    
    struct iterator : GenIterator<V> {
        CyclicVector *vec;
        Idt idx;
        
        inline iterator(const Self *const& v, const Idt i) :
        vec(const_cast<Self *const>(v)), idx(i) {};
        
        inline virtual void advance() override {
            idx++;
        }
        inline virtual void retreat() override {
            idx--;
        }
        inline virtual Value* ptr() const override {
            if(vec == nullptr) return nullptr;
            //std::cout << "\titer cmp: ptr=" << vec << ", idx=" << idx << "\n";
            //std::cout << "\titer ptr: from " << vec << " at " << idx << " = " << vec->get(idx) << "\n";
            return const_cast<Value*>((const_cast<CyclicVector*>(vec)->get(idx)));
        }
        /*inline virtual bool equal(const iterator& o) const noexcept override {
            return idx == o.idx;
        }*/
        inline virtual bool equal(const void *const& oPtr) const noexcept override {
            const iterator& o = *reinterpret_cast<const iterator*>(oPtr);
            return idx == o.idx;
        }
        inline virtual const void* iterPtr() const noexcept override {
            return reinterpret_cast<const void*>(this);
        }
    };
    
    inline const Value& operator[](const Idt id) const {
        return *this->get(id);
    }
    _rmutate(operator[],(const Idt id),(id))
    
    inline const Value& front() const {
        return *this->get(Idt(edge.leading) - 1);
    }
    inline const Value& back() const {
        return *this->get(edge.trailing);
    }
    _rmutate(front,(),());
    _rmutate(back,(),());
};

#endif
