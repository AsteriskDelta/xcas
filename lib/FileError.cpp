#include "FileError.h"
#include <iostream>

namespace Caspar {
    void FileError(const std::string& msg, const std::string& path, const unsigned int lineNumber, const std::string& line) {
        std::cerr << "Error: " << msg << std::endl << "\t" << line << std::endl << "\tfrom " << path << " at line #" << lineNumber << "\n";
    }
};
