#ifndef CYCLIC_IMPL_H
#define CYCLIC_IMPL_H

#include "CyclicVector.h"

#define CV_TPL template<typename V>
#define CV_T CyclicVector<V>

/*CV_TPL
CV_T::CyclicVector() : base(), edge(), cycleLength(0xFF) {
    
}*/
CV_TPL
CV_T::CyclicVector(typename CV_T::Idt sz) : base(), edge(), cycleLength(sz) {
    this->setMaxSize(sz);
}


CV_TPL
CV_T::~CyclicVector() {
    
}

CV_TPL
inline typename CV_T::Idx CV_T::resolveIdt(const typename CV_T::Idt idt) const {

    if(this->size() == 0) return 0;
    else if(idt < 0) return this->resolveIdt(Idt(this->size()) + idt);
    //std::cout << "RES=" <<(Idx(idt) % this->size()) << " from " << idt << " % " << this->size() << "\n";
    return (Idx(idt) % this->size());
}

CV_TPL
const typename CV_T::Value* CV_T::get(const typename CV_T::Idt id) const {
    if(this->size() == 0) return nullptr;
    const Idx idx = this->resolveIdt(id);
    //std::cout << "\t\tresolving " << id << " to idx=" << idx << ", this->size() = " << this->size() << "\n";
    return &base[idx];
}
CV_TPL
const typename CV_T::Value* CV_T::rawGet(const typename CV_T::Idx& absID) const {
    return &base[absID];
}

CV_TPL
bool CV_T::cycled() const {
    if(edge.cycleCnt == 0 && edge.fill > 0) return true;
    else return false;
}

CV_TPL
bool CV_T::recycled() const {
    return edge.fill > 0 && edge.leading == 0;
}

CV_TPL
typename CV_T::iterator CV_T::push(const typename CV_T::Value& val) {
    if(edge.max == 0) return iterator(this, 0);
    //std::cout << "pushing, trailing=" << edge.trailing << ", leading=" << edge.leading << " of " << edge.fill << "/" << edge.max << "\n";
    if(edge.fill < edge.max) {
        base.push_back(val);
    } else {
        base[edge.leading] = val;
    }
    
    //const Idx prevIdx = edge.leading;
    //const auto ret = this->end();
    edge.leading = (edge.leading + 1) % edge.max;
    edge.fill = std::min(int(edge.fill + 1), int(edge.max));
    if(edge.fill == edge.max) edge.trailing = edge.leading;
    
    edge.cycleCnt = (edge.cycleCnt + 1) % cycleLength;
    //std::cout << "\tbase sz = " << base.size() << ", edge leading=" << edge.leading << ", trailing=" << edge.trailing <<", max=" << edge.max << ", fill=" << edge.fill <<"\n";
    return this->iter(-1);
}

CV_TPL
typename CV_T::iterator CV_T::push(const Value *const ptr, const unsigned int count) {
    for(unsigned int i = 0; i < count; i++) this->push(*(ptr + i));
    return this->iter(-1);
}

CV_TPL
typename CV_T::Idt CV_T::pop() {
    if(edge.fill <= 0) return 0;
    
    edge.leading = (edge.leading - 1) % edge.max;
    edge.fill--;
    return (edge.leading - 1) % edge.max;
}

CV_TPL
typename CV_T::Idt CV_T::size() const {
    return std::min(edge.fill, edge.max);
}
CV_TPL
typename CV_T::Idt CV_T::maxSize() const {
    return edge.max;
}
CV_TPL
void CV_T::setMaxSize(const typename CV_T::Idx& sz) {
    base.reserve(sz);
    edge.max = sz;
}
CV_TPL
void CV_T::setCycleSize(const typename CV_T::Idx& sz) {
    cycleLength = sz;
}


CV_TPL
void CV_T::clear() {
    base.clear();
    edge = decltype(edge)();
}

CV_TPL
typename CV_T::iterator CV_T::begin() const {
    return iterator(this, edge.trailing);
}
CV_TPL
typename CV_T::iterator CV_T::end() const {
    if(edge.leading <= edge.trailing) return iterator(this, edge.leading + this->size());
    else return iterator(this, edge.leading);
}
CV_TPL
typename CV_T::iterator CV_T::iter(const Idt idt) const {
    return iterator(this, idt);
}
CV_TPL
typename CV_T::iterator CV_T::invalid() const {
    return this->end();//return iterator(nullptr, 0);
}

#endif
