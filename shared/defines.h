#pragma once

#include <stdint.h>
#include <cmath>
#include <string>
#include <iomanip>
#include <sstream>

#ifndef _WIN32
# include <unistd.h>
# include <utime.h>
#else
# include <direct.h>
# include <io.h>
#endif

#pragma GCC diagnostic ignored "-Wnarrowing"
//#include "approximation.h"

#define ADD(x, y) ((x) + (y))

#define DCDEBUG

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80

#define UP	BIT0
#define DOWN	BIT1
#define LEFT	BIT2
#define RIGHT	BIT3
#define FRONT	BIT4
#define BACK	BIT5

#define RAD_TO_DEG (180.0f / (float)M_PI)
#define DEG_TO_RAD ((float)M_PI / 180.0f)

#define RAD_360 (2.0 * M_PI)
#define RAD_360F ((float)(2.0 * M_PI))

#ifdef __GNUC__
#define _pure __attribute__((pure))
#define _unroll __attribute__((optimize("unroll-loops"))) 
#define _tls __thread
#define _likely(x) (__builtin_expect(!!(x), 1))
#define _unlikely(x) (__builtin_expect(!!(x), 0))
#else
#define pure [[pure]]
#endif

#define RDID() __COUNTER__

typedef uint8_t			Uint8;
typedef unsigned short		Uint16;
typedef unsigned int		Uint32;
typedef uint64_t		Uint64;

typedef int8_t			Sint8;
typedef short int		Sint16;
typedef int			Sint32;
typedef int64_t			Sint64;

typedef int8_t			Int8;
typedef short int		Int16;
typedef int			Int32;
typedef int64_t			Int64;

typedef unsigned int		GlUint;
typedef int			GlInt;

typedef unsigned long long	PtrInt;

#define MAX_Uint8	256
#define MAX_Uint16	65535

#define MAX_Int16	32767

#define _default template<class=void>
#define _defaultDef template<class>

template <class T>
inline float clamp(T x, T a, T b) {
  return x < a ? a : (x > b ? b : x);
}

template <typename T>
inline T max(T a, T b) {
  return std::max(a, b);
}

template <typename T>
inline T min(T a, T b) {
  return std::min(a, b);
}

template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

template<typename K>
inline K wrapMP(K in) {
  if(in >= (K)M_PI) return in - (K)M_PI;
  else if(in <= -(K)M_PI) return in + (K)M_PI;
  else return in;
}

static const double     _PI= 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348;
static const double _TWO_PI= 6.2831853071795864769252867665590057683943387987502116419498891846156328125724179972560696;

// Floating-point modulo
// The result (the remainder) has same sign as the divisor.
// Similar to matlab's mod(); Not similar to fmod() -   Mod(-3,4)= 1   fmod(-3,4)= -3
template<typename T>
T sfmod(T x, T y) {
  //if (0.== y) return x;
  double m= x - y * floor(x/y);
  
  // handle boundary cases resulted from floating-point cut off:
  if (y > 0) {            // modulo range: [0..y)
    if (m >= y) return 0;// Mod(-1e-16             , 360.    ): m= 360.
    else if (m < 0) {
      if(y+m == y) return 0  ; // just in case...
      else return y+m; // Mod(106.81415022205296 , _TWO_PI ): m= -1.421e-14 
    }
  } else {                  // modulo range: (y..0]
    if (m<=y) return 0;// Mod(1e-16              , -360.   ): m= -360.
    
    if (m>0 ) {
      if (y+m == y) return 0  ; // just in case...
      else return y+m; // Mod(-106.81415022205296, -_TWO_PI): m= 1.421e-14 
    }
  }
  
  return m;
}

// wrap [rad] angle to [-PI..PI)
inline double WrapPosNegPI(double fAng) {
    return sfmod(fAng + _PI, _TWO_PI) - _PI;
}

inline float wrapSPi(float fAng) {
    return sfmod(fAng + (float)_PI, (float)_TWO_PI) - (float)_PI;
}

// wrap [rad] angle to [0..TWO_PI)
inline double WrapTwoPI(double fAng) {
    return sfmod(fAng, _TWO_PI);
}

// wrap [deg] angle to [-180..180)
inline double WrapPosNeg180(double fAng) {
    return sfmod(fAng + 180., 360.) - 180.;
}

// wrap [deg] angle to [0..360)
inline double Wrap360(double fAng) {
    return sfmod(fAng ,360.);
}

template<typename K>
inline K lerpAngle(K u, K v, K p) {
    return u + p*wrapSPi(v - u);
}

union Int2x3 {
     struct { 
        unsigned int a:2;
        unsigned int b:2;
	unsigned int c:2;
	bool d:1;
     };
     struct { 
        char x:2;
        char y:2;
	char z:2;
	bool w:1;
     };
     unsigned char raw;
};

union Int4x2 {
     struct { 
        unsigned int a:4;
        unsigned int b:4;
     };
     struct { 
        char p:4;
        char m:4;
     };
     struct { 
        char s:4;
        char e:4;
     };
     unsigned char raw;
     
     inline bool operator== (const Int4x2 mo) const { return raw == mo.raw; };
};

union Int4x4 {
     struct { 
        unsigned int a:4;
        unsigned int b:4;
	unsigned int c:4;
        unsigned int d:4;
     };
     struct { 
        char x:4;
        char y:4;
	char z:4;
	char w:4;
     };
     unsigned short raw;
};

union Int4x6 {
     struct { 
        unsigned int a:4;
        unsigned int b:4;
	unsigned int c:4;
        unsigned int d:4;
	unsigned int e:4;
	unsigned int f:4;
     };
     struct { 
        char x:4;
        char y:4;
	char z:4;
	char w:4;
	char i:4;
	char j:4;
     };
     unsigned int raw : 24;
};

#ifdef __cplusplus
    #define PCAST(type, pointer) reinterpret_cast<type>(pointer)
#else
    #define PCAST(type, pointer) (type)(pointer)
#endif

struct Rect {
  float x, y, w, h;
};

struct V2 {
  float x, y;
  
  inline V2 operator+(const V2 o) const { return V2{x + o.x, y + o.y}; };
  inline V2 operator-(const V2 o) const { return V2{x - o.x, y - o.y}; };
  inline V2 operator*(const float m) const { return V2{x * m, y * m};  };
  
  inline V2 operator*(const V2 o) const { return V2{x * o.x, y * o.y};  };
  
  inline bool operator< (const float m) const { return (x + y) < m; };
  inline bool operator> (const float m) const { return (x + y) > m; };
  
  inline void rotate(const float d) {
    const float theta =d * DEG_TO_RAD;
    float cs = cos(theta), sn = sin(theta), px, py;
    px = x * cs - y * sn; 
    py = x * sn + y * cs;
    x = px; y = py;
  }
  
  inline float magnitude() const { return sqrt(x * x + y * y); };
  inline V2 normalized() const { float m = magnitude(); return V2{x/m, y/m}; };
  
  inline float distanceTo(V2 o) {
    return sqrt(pow(x - o.x, 2) + pow(y - o.y, 2));
  }
  
  inline void operator+=(const V2 o) { x += o.x; y += o.y; };
  inline void operator-=(const V2 o) { x -= o.x; y -= o.y; };
  
  inline void operator*=(const float o) { x *= o; y *= o; };
  inline void operator/=(const float o) { x /= o; y /= o; };
  
  inline std::string ToString() { std::stringstream ss; ss << "(" << x << ", " << y << ")"; return ss.str(); };
};

template<class T>
class V2T {
public:
  T x, y;
  
  
  inline V2T operator+(const V2T o) const { return V2T{x + o.x, y + o.y}; };
  inline V2T operator-(const V2T o) const { return V2T{x - o.x, y - o.y}; };
  inline V2T operator+(const T o) const { return V2T{x + o, y + o}; };
  inline V2T operator-(const T o) const { return V2T{x - o, y - o}; };
  inline V2T operator*(const T m) const { return V2T{x * m, y * m};  };
  
  inline bool operator< (const T m) const { return (x + y) < m; };
  inline bool operator> (const T m) const { return (x + y) > m; };
  
  inline T magnitude() const { return sqrt(x * x + y * y); };
  inline V2T normalized() const { T m = magnitude(); return V2T{x/m, y/m}; };
  
  inline void operator+=(const V2T o) { x += o.x; y += o.y; };
  inline void operator-=(const V2T o) { x -= o.x; y -= o.y; };
  
  inline void operator*=(const T o) { x *= o; y *= o; };
  inline void operator/=(const T o) { x /= o; y /= o; };
  
  inline std::string ToString() { std::stringstream ss; ss << "(" << x << ", " << y << ")"; return ss.str(); };
};

template<class T>
class V3T {
public:
  T x, y, z;
  
  
  inline V3T operator+(const V3T o) const { return V3T{x + o.x, y + o.y, z + o.z}; };
  inline V3T operator-(const V3T o) const { return V3T{x - o.x, y - o.y, z - o.z}; };
  inline V3T operator+(const T o) const { return V3T{x + o, y + o, z + o}; };
  inline V3T operator-(const T o) const { return V3T{x - o, y - o, z - o}; };
  inline V3T operator*(const T m) const { return V3T{x * m, y * m, z * m};  };
  inline V3T operator/(const T m) const { return V3T{x / m, y / m, z / m};  };
  
  inline bool operator< (const T m) const { return magnitude() < m; };
  inline bool operator> (const T m) const { return magnitude() > m; };
  
  inline T magnitude() const { return sqrt(x * x + y * y + z * z); };
  inline V3T normalized() const { T m = magnitude(); return V3T{x/m, y/m, z/m}; };
  
  inline bool Within(T min, T max) const { return (x >= min && x <= max && y >= min && y <= max && z >= min && z <= max); };
  
  inline void operator+=(const V3T o) { x += o.x; y += o.y; z += o.z; };
  inline void operator-=(const V3T o) { x -= o.x; y -= o.y; z -= o.z; };
  
  inline void operator*=(const T o) { x *= o; y *= o; z *= o; };
  inline void operator/=(const T o) { x /= o; y /= o; z *= o;};
  
  const inline std::string ToString() { std::stringstream ss; ss << "V3T(" << ((float)x) << ", " << ((float)y) << ", " << ((float)z) << ")"; return ss.str(); };
};

#pragma GCC diagnostic warning "-Wnarrowing"

#define COLOR_DECLARED
struct Color {
  unsigned char r, g, b, a;
  
  Color() : r(0), g(0), b(0), a(255) {};
  Color(unsigned char rn, unsigned char gn, unsigned char bn, unsigned char an = 255) : r(rn), g(gn), b(bn), a(an) {};
  Color(std::string hex) {
    //Prepare hex string
    if(hex[0] == '0'&&hex[1] == 'x') hex = hex.substr(2, hex.length() - 2);
    if(hex.length() < 6) return;
    
    int rawColor = (int)strtol(hex.c_str(), NULL, 16);
    
    if(hex.length() >= 8) {//Handle alpha as well
      r = (unsigned char)((rawColor & 0xFF000000) >> 24);
      g = (unsigned char)((rawColor & 0x00FF0000) >> 16);
      b = (unsigned char)((rawColor & 0x0000FF00) >> 8);
      a = (unsigned char)(rawColor & 0x000000FF);
    } else {
      r = (unsigned char)(rawColor & 0xFF0000);
      g = (unsigned char)(rawColor & 0x00FF00);
      b = (unsigned char)(rawColor & 0x0000FF);
      a = 255;
    }
  }
  
  inline Color LerpTo(Color o, float t) {
    Color ret; const float m = 1.0f - t;
    ret.r = t * r + o.r * m;
    ret.g = t * g + o.g * m;
    ret.b = t * b + o.b * m;
    ret.a = t * a + o.a * m;
    return ret;
  }
  
  inline std::string Hex() {
    std::stringstream ss;
    ss << std::hex << std::setw(2) << std::setfill('0') << ((int)r)<< ((int)g)<< ((int)b)<< ((int)a);
    return ss.str(); 
  }
  
  inline std::string ToString() { std::stringstream ss; ss << "Color(" << ((int)r) << ", " << ((int)g) << ", " <<((int)b) << ", " <<((int)a) << ")"; return ss.str(); };
};

struct Color3 {
  unsigned char r, g, b;
  
  Color3() : r(0), g(0), b(0) {};
  Color3(unsigned char rn, unsigned char gn, unsigned char bn) : r(rn), g(gn), b(bn) {};
  Color3(std::string hex) {
    //Prepare hex string
    if(hex[0] == '0'&&hex[1] == 'x') hex = hex.substr(2, hex.length() - 2);
    if(hex.length() < 6) return;
    
    int rawColor = (int)strtol(hex.c_str(), NULL, 16);
    
    if(hex.length() >= 8) {//Handle alpha as well
      r = (unsigned char)((rawColor & 0xFF000000) >> 24);
      g = (unsigned char)((rawColor & 0x00FF0000) >> 16);
      b = (unsigned char)((rawColor & 0x0000FF00) >> 8);
    } else {
      r = (unsigned char)(rawColor & 0xFF0000);
      g = (unsigned char)(rawColor & 0x00FF00);
      b = (unsigned char)(rawColor & 0x0000FF);
    }
  }
  
  inline Color3 LerpTo(Color3 o, float t) {
    Color3 ret; const float m = 1.0f - t;
    ret.r = t * r + o.r * m;
    ret.g = t * g + o.g * m;
    ret.b = t * b + o.b * m;
    return ret;
  }
  
  inline std::string Hex() {
    std::stringstream ss;
    ss << std::hex << std::setw(2) << std::setfill('0') << ((int)r)<< ((int)g)<< ((int)b);
    return ss.str(); 
  }
  
  inline std::string ToString() { std::stringstream ss; ss << "Color3(" << ((int)r) << ", " << ((int)g) << ", " <<((int)b) << ")"; return ss.str(); };
};

struct FloatColor {
  float r, g, b, a;
  
  inline FloatColor() { r = g = b = a = 0.0f; };
  inline FloatColor(float nR, float nG, float nB, float nA = 1.0) {
    r = nR; g = nG; b = nB; a = nA;
  }
  
  inline FloatColor(const Color& c) {
    r = (float)c.r / 255.0f;
    g = (float)c.g / 255.0f;
    b = (float)c.b / 255.0f;
    a = (float)c.a / 255.0f;
  };
  
  inline void operator=(const Color& c) {
    r = (float)c.r / 255.0f;
    g = (float)c.g / 255.0f;
    b = (float)c.b / 255.0f;
    a = (float)c.a / 255.0f;
  };
};

template <class TL>
inline TL lerp(TL a, TL b, float m) {
  m = clamp(m, 0.0f, 1.0f);
  return a * (1.0f - m) + b * m;
}
