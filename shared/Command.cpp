#include "Command.h"

std::unordered_map<std::string, CommandMap> CommandManager::commands;
unsigned short CommandManager::count;

bool CommandManager::Register(const std::string& cmd, COMMANDFUNC_RET (*func)COMMANDFUNC_ARGS, const std::string& desc, unsigned short argNum, std::string* args, unsigned short mod) {
  
  if(commands.find(cmd) != commands.end()) return false;
  
  CommandMap cStruct;
  cStruct.command = cmd;
  cStruct.shortDesc = desc;
  cStruct.args = args;
  cStruct.argCount = argNum;
  cStruct.function = func;
  
  cStruct.modID = mod;
  
  commands.insert({cmd, cStruct});
  return true;
}

int CommandManager::Call(std::string& desired, std::string& feedback) {
  std::string rawCmd;
  std::string* args = NULL;
  CommandMap* cmd = NULL;
  
  unsigned short desiredLength = desired.length();
  unsigned short segment = 0;
  unsigned short pos = 0; unsigned short segStart = 0; bool quoted= false;
  
  char character;
  while(pos <= desiredLength) {
    if(pos < desiredLength) character = desired[pos];
    else character = ' ';
    if(quoted) {
      if(character == '"'&&(pos == 0||desired[pos-1] != '\\')) quoted = false;
    } else {
      if(character == '"'&&(pos == 0||desired[pos-1] != '\\')) quoted = true;
      else if(character == ' ') {
	if(segment == 0) {
	  rawCmd = desired.substr(segStart, pos - segStart);
	  
	  if(commands.find(rawCmd) == commands.end()) {
	    feedback = ": Command not found!";
	    feedback.insert(0, rawCmd);
	    return -2;
	  }
	  cmd = &(commands.find(rawCmd))->second;
	  args = new std::string[cmd->argCount];
	  segStart = pos + 1;
	} else {
	  if(desired[segStart] == '"'&&desired[pos - 1] == '"') {
	    args[segment - 1] = desired.substr(segStart + 1, pos - segStart - 2);
	  } else {
	    args[segment - 1] = desired.substr(segStart, pos - segStart);
	  }
	  segStart = pos + 1;
	  
	  if(args[segment - 1] == "") { pos++; continue;}
	}
	
	//This works because the first /segment/ is actually the command
	segment++;
	if(segment > cmd->argCount) break;
	
      }
    }
    pos++;
  }
  int status = -1;
  //Now call the function
  if(cmd != NULL && cmd->modID == 0) {
    status = (*(cmd->function))(rawCmd, cmd->argCount, args, feedback);
  } else {
    
  }
  
  return status;
}