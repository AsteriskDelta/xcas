#include "error.h"
#include <iostream>
#include <cmath>
#include <cstdlib>

void ErrorProxy::ThrowFatal(std::string text) {
	ThrowLevel(ERR_FATAL, text);
}

void ErrorProxy::ThrowFatal(std::string text, std::string replace) {
	ThrowLevel(ERR_FATAL, text, replace);
}

void ErrorProxy::ThrowSevere(std::string text) {
	ThrowLevel(ERR_SEVERE, text);
}

void ErrorProxy::ThrowSevere(std::string text, std::string replace) {
	ThrowLevel(ERR_SEVERE, text, replace);
}

void ErrorProxy::Warn(std::string text) {
	ThrowLevel(ERR_WARNING, text);
}

void ErrorProxy::Warn(std::string text, std::string replace) {
	ThrowLevel(ERR_WARNING, text, replace);
}

void ErrorProxy::Message(std::string text) {
	ThrowLevel(ERR_MESSAGE, text);
}

void ErrorProxy::Message(std::string text, std::string replace) {
	ThrowLevel(ERR_MESSAGE, text, replace);
}

void ErrorProxy::Message(std::string text, int replace) {
	std::stringstream newReplace;
	newReplace << replace;
	ThrowLevel(ERR_MESSAGE, text, newReplace.str());
}

inline void ErrorProxy::ThrowLevel(int level, std::string text, std::string replace) {
	unsigned long replaceOffset = text.find("%r");
	if(replaceOffset != std::string::npos) text.replace(replaceOffset, 2, replace); 
	ThrowLevel(level, text);
}

inline void ErrorProxy::ThrowLevel(int level, std::string text) {
  if(requiresClear) std::cout << "\033[2K";
  
	switch(level) {
		case ERR_FATAL:
			std::cerr << "[Fatal Error] " << text << std::endl;
			exit(1);
			break;
		case ERR_SEVERE:
			std::cerr << "[Severe] " << text << std::endl;
			break;
		case ERR_WARNING:
			std::cerr << "[Warning] " << text << std::endl;
			break;
		case ERR_MESSAGE:
			std::cout << text << std::endl;
			break;
		default:
			std::cout << text << std::endl;
			break;
	}
	
	requiresClear = false;
}


void ErrorProxy::Progress(std::string text, int current, int end, int start) {
  float progress = float(current) / float(end - start);
  //Show two decimal points
  progress *= 10000.0f; progress = round(progress); progress /= 100.0f;
  std::cout << "[" << progress << "%] " << text << "\r";
  std::cout.flush();
  
  requiresClear = true;
}
