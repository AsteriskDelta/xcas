#pragma once

#include <cstring>
#include <string>
#include <sstream>
#include <climits>

#define GAMENAME "BDHack [Alpha]"
#ifndef _unused
#define _unused(x) ((void)x)
#endif

#include "defines.h"
#include "error.h"

#include "../lib/Random.h"
#include "../lib/Vector2.h"
#include "../lib/Vector3.h"

#include "../lib/Console.h"
#include "../lib/SaveData.h"
#include "../lib/GameData.h"
#include "../lib/Audio.h"

#include "Command.h"
#include "Mod.h"