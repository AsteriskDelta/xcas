#pragma once

#ifndef ERR_FATAL 
#define ERR_FATAL 1
#define ERR_SEVERE 2
#define ERR_WARNING 3
#define ERR_MESSAGE 0

#include <string>
#include <sstream>


class ErrorProxy {
	public:
		void ThrowFatal(std::string text);
		void ThrowFatal(std::string text, std::string replace);
		
		void ThrowSevere(std::string text);
		void ThrowSevere(std::string text, std::string replace);
		
		void Warn(std::string text);
		void Warn(std::string text, std::string replace);
		
		void Message(std::string text);
		void Message(std::string text, std::string replace);
		void Message(std::string text, int replace);
		
		void Progress(std::string text, int current, int end, int start = 0);
	private:
		inline void ThrowLevel(int level, std::string text);
		inline void ThrowLevel(int level, std::string text, std::string replace);
		
		bool requiresClear;
};

#endif