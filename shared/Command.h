#pragma once
#include <unordered_map>
#include <string>

#define COMMANDFUNC_ARGS (std::string&, 	unsigned short, 	 std::string*, 	    std::string& output)
#define COMMANDFUNC_DEF (std::string& command,  unsigned short argCount, std::string* args, std::string& output)
#define COMMANDFUNC_RET int

struct CommandMap {
  CommandMap() { modID = argCount = 0;};
   
  std::string command;
  std::string shortDesc;
  
  std::string* args;
  unsigned short argCount;
  unsigned short modID;
  
  COMMANDFUNC_RET (*function)COMMANDFUNC_ARGS;
};

class CommandManager {
public:
  static std::unordered_map<std::string, CommandMap> commands;
  static unsigned short count;
  
  static bool Register(const std::string& cmd, COMMANDFUNC_RET (*func)COMMANDFUNC_ARGS, const std::string& desc, unsigned short argNum, std::string* args, unsigned short mod = 0);
  
  static int Call(std::string& desired, std::string& feedback);
private:
  
};