#include <string>
#include "../lib/GameData.h"
#include "../lib/VersionCode.h"

#ifndef LUA_OBJECT_TYPE
  #define LUA_OBJECT_TYPE void*
#endif

enum ModTarget { MOD_CLIENT, MOD_SERVER, MOD_UNI };

class Mod {
public:
  std::string name;
  std::string description;
  
  VersionCode version;
  
  VersionCode minGame;
  VersionCode maxGame;
  
  GameData data;
  
  bool Load(std::string path);
  
  static bool LoadAll();
private:
  bool restrict;
  bool authorized;
  
  ModTarget target;
  
  std::string path;
  LUA_OBJECT_TYPE luaObject;
};