class GenericEntity {
public:
  ENTITY_ID_TYPE id;		//Server-assigned ID
  Species* species;	//Because class is a keyword, 2 byte serialization
  
  Uint8 level;
  
  //Position, always sent, 0xFFFF if invisible
  Position pos;
  
  Uint8 control;
  
  /* Below serialized to four bytes if required by control bit 0
   * | #   #   #   #   #   #   #   # | #   #   #   #   #   #   #   # |
   * | gender| align | hairColor     | eyeColor      | soulColor     |
   * 
   * | #   #   #   #   #   #   #   # | #   #   #   #   #   #   #   # |
   * | skinColor     |  hairStyle    |   height      |   weight      |
   */
  Uint8  gender;
  Uint8  alignment;
  ShortColor hairColor;
  ShortColor eyeColor;
  ShortColor soulColor;
  ShortColor skinColor;
  Uint8 hairStyle;
  Uint8 height;
  Uint8 weight;
  
  //Included if ctrl1 is set
  Uint16 hp;
  Uint16 maxhp;
  
  //Included if ctrl2 is set
  Uint16 mana;
  Uint16 maxMana;
  
  //experience, ctrl3
  Uint16 xp;
  Uint16 points;
  
  //Utility variables
  Uint8 serialSize;
  
protected:
  inline void UpdateSerialSize();
};

inline void GenericEntity::UpdateSerialSize() {
  serialSize = ADD(POSITION_SIZE, ADD(sizeof(ENTITY_ID_TYPE), 3));//ID size plus ctrl and species
  
  if((control & BIT0) != 0) serialSize += 4;//Are we to include metadata?
  if((control & BIT1) != 0) serialSize += 4;//Are we to include hp data?
  if((control & BIT2) != 0) serialSize += 4;//Are we to include mana data?
  if((control & BIT3) != 0) serialSize += 4;//Are we to include experience/level data?
}