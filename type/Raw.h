#ifndef XCAS_TYPE_RAW_H
#define XCAS_TYPE_RAW_H
#include "Type.h"

namespace Caspar {
  namespace Types {
    class Raw : public Type {
    public:
      Raw();
      virtual ~Raw();

      virtual bool matches(const XNode *const node, bool rawMatch = false) const override;
      virtual XNode *blankClone() const override;

      virtual const std::string& typeName() const override;
    protected:

    };
  };
};
#endif
