#ifndef INC_CASPAR_BOOLEAN_H
#define INC_CASPAR_BOOLEAN_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class Boolean : public Symbol {
    public:
        Boolean();
        virtual ~Boolean();
        
        bool value;
        bool chained;//True if the bool may represent a different type altogether when true
    protected:
        
    };
};

#endif




