#include "Property.h"

namespace Caspar {
  namespace Types {
    Property::Property() {

    }
    Property::~Property() {

    }

    XNode* Property::blankClone() const {
      return new Property();
    }

    const std::string& Property::typeName() const {
      static const std::string tName = "property";
      return tName;
    }
  }
}
