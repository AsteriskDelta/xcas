#include "Block.h"

namespace Caspar {
    Block::Block() : Symbol(), Scope(), nodes() {

    }
    Block::~Block() {

    }

    const std::string& Block::typeName() const {
        static const std::string tName = "block";
        return tName;
    }

    void Block::addNode(XNode *node) {
        nodes.push_back(node);
    }

    XNode* Block::blankClone() const {
        return new Block();
    };
    void Block::mirror(const XNode *const o) {
        //TODO
        XNode::mirror(o);
    }
    bool Block::matches(const XNode *const o, bool rawMatch) const {
        return XNode::matches(o, rawMatch);
    }

    bool Block::executeOn(XNode *node, VM *const vm) {

        return true;
    }
}
