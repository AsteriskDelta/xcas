#ifndef INC_CASPAR_IRRATIONAL_H
#define INC_CASPAR_IRRATIONAL_H
#include "iCaspar.h"
#include "Number.h"
#include <gmpxx.h>

namespace Caspar {
    class Irrational : public Number {
    public:
        Irrational();
        virtual ~Irrational();
        
        mpf_class approx;
        
        virtual void generate(unsigned int precision);//In bits
    protected:
        
    };
};

#endif



