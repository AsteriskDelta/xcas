#include "Delimiters.h"

namespace Caspar {
  namespace Types {
    SoE::SoE() {

    }
    SoE::~SoE() {

    }

    bool SoE::matches(const XNode *const node, bool rawMatch) const {
        _unused(rawMatch);
      return node->type == XNode::Delimiter;
    }

    const std::string& SoE::typeName() const{
        static const std::string tName = "soe";
        return tName;
    }

    XNode* SoE::blankClone() const {
      return new SoE();
    }

    EoE::EoE() {

    }
    EoE::~EoE() {

    }

    bool EoE::matches(const XNode *const node, bool rawMatch) const {
        _unused(rawMatch);
        std::cout << "EOE MATCH? " << node->infoString() << "\n";
      return node->type == XNode::Delimiter;
    }

    const std::string& EoE::typeName() const{
        static const std::string tName = "eoe";
        return tName;
    }
    XNode* EoE::blankClone() const {
      return new EoE();
    }
  };
};
