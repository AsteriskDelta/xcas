#ifndef INC_CASPAR_RATIONAL_H
#define INC_CASPAR_RATIONAL_H
#include "iCaspar.h"
#include "Number.h"
#include <gmpxx.h>

namespace Caspar {
    class Rational : public Number {
    public:
        Rational();
        virtual ~Rational();
        
        mpq_class fract;
    protected:
        
    };
};

#endif



