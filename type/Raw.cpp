#include "Raw.h"

namespace Caspar {
  namespace Types {
    Raw::Raw() {

    }
    Raw::~Raw() {

    }

    bool Raw::matches(const XNode *const node, bool rawMatch) const {
        _unused(rawMatch);
        if(node->type == (XNode::Delimiter)) return false;
        else return true;
    }

    XNode* Raw::blankClone() const {
      return new Raw();
    }

    const std::string& Raw::typeName() const {
      static const std::string tName = "raw";
      return tName;
    }
  }
}
