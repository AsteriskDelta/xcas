#ifndef XCAS_TYPE_H
#define XCAS_TYPE_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
  class Type : public Symbol {
  public:
    Type();
    virtual ~Type();

    virtual bool matches(const XNode *const node, bool rawMatch = false) const override;

    virtual const std::string& typeName() const;

    void registerTo(Scope *tScope);
  protected:

  };
};

#endif
