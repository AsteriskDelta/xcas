#ifndef INC_CASPAR_PROPERTY_H
#define INC_CASPAR_PROPERTY_H
#include "iCaspar.h"
#include "XNode.h"
#include "Type.h"

namespace Caspar {
    namespace Types {
        class Property : public Type {
        public:
            Property();
            virtual ~Property();

            virtual XNode *blankClone() const override;

            virtual const std::string& typeName() const override;
        protected:

        };
    };
};

#endif
