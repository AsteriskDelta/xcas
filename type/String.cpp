#include "String.h"

namespace Caspar {
    namespace Types {
    String::String() : Type(), contents() {
        this->setType(XNode::Literal);
        this->setFlag(XNode::Resolved);
    }
    String::String(const std::string& val) : Type(), contents(val) {
        this->setType(XNode::Literal);
        this->setFlag(XNode::Resolved);
    }

    String::~String() {

    }

    const std::string& String::typeName() const {
      static const std::string tn = "string";
      return tn;
    }

    std::string String::raw() const {
        return contents;
    }

    std::string String::str() const {
        return contents;
    }

    XNode* String::blankClone() const {
        return new String();
    };
    void String::mirror(const XNode *const o) {
        //std::cout << "mirror() invoked on string " << this->str() << " -> " << o->str() << "\n";
        this->contents = o->str();
        //Symbol::state.real = true;
        XNode::mirror(o);
    }
    bool String::matches(const XNode *const o, bool rawMatch) const {
        return XNode::matches(o, rawMatch) && this->raw() == o->raw();
    }

    bool String::executeOn(XNode *node, VM *const vm) {
        std::stringstream ss;
        //bool first = true;
        for(auto child = node->firstChild; child != nullptr; child = child->next) {
            //if(!first) ss << " ";
            ss << child->str();
        }

        String *ret = new String(ss.str());
        node->replaceWith(ret);
        delete node;
        
        return true;
    }
  };
};
