#ifndef INC_CASPAR_ABSTRACT_H
#define INC_CASPAR_ABSTRACT_H
#include "iCaspar.h"
#include "Number.h"

namespace Caspar {
    class Abstract : public Number {
    public:
        Abstract();
        virtual ~Abstract();
        
    protected:
        
    };
};

#endif




