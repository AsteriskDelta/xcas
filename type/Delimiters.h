#ifndef XCAS_TYPE_DELIM_H
#define XCAS_TYPE_DELIM_H
#include "Type.h"

namespace Caspar {
  namespace Types {
    class SoE : public Type {
    public:
      SoE();
      virtual ~SoE();

      virtual bool matches(const XNode *const node, bool rawMatch = false) const override;
      virtual XNode *blankClone() const override;

      virtual const std::string& typeName() const override;
    protected:

    };

    class EoE : public Type {
    public:
      EoE();
      virtual ~EoE();

      virtual bool matches(const XNode *const node, bool rawMatch = false) const override;
      virtual XNode *blankClone() const override;

      virtual const std::string& typeName() const override;
    protected:

    };
  };
};
#endif
