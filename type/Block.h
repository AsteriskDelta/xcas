#ifndef INC_CASPAR_BLOCK_H
#define INC_CASPAR_BLOCK_H
#include "iCaspar.h"
#include "Symbol.h"
#include "Scope.h"

namespace Caspar {
    class Block : public Symbol, public Scope {
    public:
        Block();
        virtual ~Block();

        std::list<XNode*> nodes;
        void addNode(XNode *node);

        virtual const std::string& typeName() const override;

        virtual XNode *blankClone() const override;
        virtual void mirror(const XNode *const o) override;
        virtual bool matches(const XNode *const o, bool rawMatch = false) const override;

        virtual bool executeOn(XNode *sym, VM *const vm) override;
    protected:

    };
};

#endif
