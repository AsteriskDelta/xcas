#ifndef INC_CASPAR_NUMBER_H
#define INC_CASPAR_NUMBER_H
#include "iCaspar.h"
#include "Symbol.h"
#include <gmpxx.h>

namespace Caspar {
    class Number : public Symbol {
    public:
        Number();
        virtual ~Number();

    protected:
        
    };
};

#endif


