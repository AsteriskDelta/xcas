#include "List.h"
#include <sstream>

namespace Caspar {
    namespace Types {
        List::List() : Type() {

        }
        List::~List() {

        }

        std::string List::raw() const {
            std::stringstream ss;

            bool first = true;
            for(auto it = this->begin(); it; ++it) {
                if(!first) {
                    ss << ", ";
                } else first = false;

                ss << it->raw();
            }

            return ss.str();
        }
        std::string List::str() const {
            std::stringstream ss;

            bool first = true;
            std::cout << "List begin: " << this->begin().current << " -> " << ((XNode*)this->begin()) << "\n";
            for(auto it = this->begin();
            it;
            ++it) {
                if(!first) {
                    ss << ", ";
                } else first = false;

                ss << it->str();
            }

            return ss.str();
        }

        const std::string& List::typeName() const {
            static const std::string tn = "list";
            return tn;
        }

        XNode* List::blankClone() const {//Just creates the (most derived) object (NOTE: MUST be overridden by subclasses)
            return new List();
        }
        void List::mirror(const XNode *const o) {// mirror all characteristics of the other XNode (NOTE: MUST be overridden by subclasses)
            if(this->firstChild != nullptr) delete this->firstChild;
            this->firstChild = nullptr;

            if(o->firstChild != nullptr) this->addChild(o->firstChild->clone(true,true));

            XNode::mirror(o);
        }
        bool List::matches(const XNode *const o, bool rawMatch) const {
            _unused(rawMatch);
            return XNode::matches(o, rawMatch) && this->typeName() == o->typeName();
        }

        unsigned int List::size() const {
            if(firstChild == nullptr) return 0;
            else return firstChild->size();
        }

        void List::push(XNode *const node) {
            XNode *container = XNode::MakeContainer();
            container->addChild(node);
            return this->addChild(container);
        }
    }
}
