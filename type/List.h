#ifndef INC_CASPAR_LIST_H
#define INC_CASPAR_LIST_H
#include "iCaspar.h"
#include "Type.h"

namespace Caspar {
  namespace Types {
    class List : public Type {
    public:
        List();
        virtual ~List();

        virtual std::string raw() const override;
        virtual std::string str() const override;

        virtual const std::string& typeName() const override;

        virtual XNode *blankClone() const override;//Just creates the (most derived) object (NOTE: MUST be overridden by subclasses)
        virtual void mirror(const XNode *const o) override;// mirror all characteristics of the other XNode (NOTE: MUST be overridden by subclasses)
        virtual bool matches(const XNode *const o, bool rawMatch = false) const override;

        virtual unsigned int size() const;
        //virtual XNode* operator[](const unsigned int idx) override;
        void push(XNode *const node);

        struct iterator {
            XNode *current;
            inline iterator(XNode *n) : current(n) {
            }

            inline operator XNode* () const {
                if(current == nullptr) return nullptr;
                return current->firstChild;
            }

            inline iterator& operator++() {
                if(current == nullptr) return *this;

                if(current->next != nullptr) {
                    current = current->next;
                }  else {//Recurse upwards until we find another node to traverse
                    current = nullptr;
                }
                return *this;
            }
            inline operator bool() const {
                return current != nullptr;
            }
            inline XNode* operator->() const {
                if(current == nullptr) return nullptr;
                return current->firstChild;
            }
        };

        inline virtual iterator begin() const {
            return iterator(this->firstChild);
        }
    protected:

    };
  };
};

#endif
