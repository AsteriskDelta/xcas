#include "Type.h"

namespace Caspar {
  Type::Type() {
    this->setType(XNode::Identifier);
    this->setFlag(XNode::Resolved);
  }
  Type::~Type() {

  }

  bool Type::matches(const XNode *const node, bool rawMatch) const {
    _unused(node, rawMatch);
    return false;
  }

  const std::string& Type::typeName() const{
      static const std::string tName = "type";
      return tName;
  }

  void Type::registerTo(Scope *tScope) {
    return this->name(this->typeName(), tScope);
  }
}
