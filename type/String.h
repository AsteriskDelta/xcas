#ifndef INC_CASPAR_STRING_H
#define INC_CASPAR_STRING_H
#include "iCaspar.h"
#include "Symbol.h"
#include "Type.h"

namespace Caspar {
  namespace Types {
    class String : public Type {
    public:
        String();
        String(const std::string& val);
        virtual ~String();

        virtual std::string raw() const override;
        virtual std::string str() const override;

        virtual const std::string& typeName() const override;

        virtual XNode *blankClone() const override;//Just creates the (most derived) object (NOTE: MUST be overridden by subclasses)
        virtual void mirror(const XNode *const o) override;// mirror all characteristics of the other XNode (NOTE: MUST be overridden by subclasses)
        virtual bool matches(const XNode *const o, bool rawMatch = false) const override;

        virtual bool executeOn(XNode *node, VM *const vm) override;

        std::string contents;
    protected:

    };
  };
};

#endif
