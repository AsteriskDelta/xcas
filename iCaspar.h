#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <Spinster/Spinster.h>
#include "defines.h"
#include "lib/FileError.h"

namespace Caspar {
    class PatternGroup;
    class Pattern;
    class Line;
    class ParserStream;
    class ParserFileStream;
    
    class Expression;
    class GlobalScope;
    class Scope;
    class Specials;
    class Symbol;
    class XNode;
    
    class Type;
};
