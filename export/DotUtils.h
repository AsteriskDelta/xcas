#ifndef INC_DOTUTILS_H
#define INC_DOTUTILS_H
#include "iCaspar.h"

namespace Caspar {
    namespace Dot {
        std::string Escape(const std::string& in);
    };
};

#endif

