#include "DotUtils.h"

namespace Caspar {
    namespace Dot {
        std::string Escape(const std::string& in) {
            std::string ret;
            ret.reserve(in.size() + 1 + in.size()/4);
            
            for(unsigned int i = 0; i < in.size(); i++) {
                const char c = in[i];
                switch(c) {
                    case '"':
                        ret.push_back('\\');
                        ret.push_back(c);
                        break;
                    case '\\':
                        ret.push_back('\\');
                        ret.push_back('\\');
                        break;
                    default:
                        ret.push_back(c);
                        break;
                }
            }
            return ret;
        }
    }
};
