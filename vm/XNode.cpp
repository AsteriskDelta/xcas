#include "XNode.h"
#include <sstream>
#include <fstream>
#include "DotUtils.h"
#include "VM.h"
#include "Symbol.h"

namespace Caspar {

    unsigned int XNode::size() const {
        //if(prev != nullptr) return prev->size();
        //else if(next == nullptr) return 1;
        //else return next->size() + 1;
        return this->last()->index() + 1;
    }
    unsigned int XNode::index() const {
        if(prev == nullptr) return 0;
        else return prev->index() + 1;
    }
    XNode* XNode::operator[](const unsigned int idx) {
        const unsigned int cur = this->index();
        if(cur == idx) return this;
        else if(cur < idx && next != nullptr) return (*next)[idx];
        else if(cur > idx && prev != nullptr) return (*prev)[idx];
        else return nullptr;
    }

    unsigned int XNode::depth() const {
        if(parent == nullptr) return 0;
        else return parent->depth() + 1;
    }

    XNode* XNode::root() const {
        if(parent != nullptr) return parent->root();
        else if(prev != nullptr) return prev->root();
        else return const_cast<XNode*>(this);
    }

    XNode* XNode::first() const {
        if(prev == nullptr) return const_cast<XNode*>(this);
        else return prev->first();
    }
    XNode* XNode::last() const {
        if(next == nullptr) return const_cast<XNode*>(this);
        else return next->last();
    }

    XNode* XNode::children() {
        return this->firstChild;
    }

    void XNode::unlinkTokens() {
        this->token = nullptr;
        if(this->next != nullptr) this->next->unlinkTokens();
        if(this->firstChild != nullptr) this->firstChild->unlinkTokens();
    }

    XNode::XNode() : XNode_Root(), rawTypeInfo(0x0), parent(nullptr), prev(nullptr), next(nullptr), firstChild(nullptr), symbolPtr(nullptr), token(nullptr) {
#ifdef CASPAR_XNODE_DBG
        static unsigned int debugIdenCounter = 0;
        this->debugIden = debugIdenCounter++;
#endif
    }

    XNode::~XNode() {
        if(next != nullptr) delete next;
        if(firstChild != nullptr) delete firstChild;
        if(this->token != nullptr) this->token->release();
    }

    void XNode::setType(uint16_t newType) {
        this->type = newType;
    }

    void XNode::addChild(XNode *const child) {
        for(XNode *node = child; node != nullptr; node = node->next) node->parent = this;//Set all node's parent flags correctly

        if(firstChild == nullptr) {
            firstChild = child;
            firstChild->prev = nullptr;//Don't carry over the previous chain (it doesn't belong to us)
        } else firstChild->append(child);
    }

    void XNode::append(XNode *const node) {
        if(next == nullptr) {
            next = node;
            node->prev = this;
            node->parent = this->parent;
        } else next->append(node);
    }

    void XNode::relinquishTo(XNode *const sym) {
        if(this->next != nullptr) this->next->prev = sym;
        if(this->prev != nullptr) this->prev->next = sym;

        if(sym != nullptr) {
            sym->prev = this->prev;
            sym->next = this->next;

            sym->parent = this->parent;
            if(sym->parent != nullptr && sym->parent->firstChild == this) sym->parent->firstChild = sym;//Reassign first child if needed

            sym->firstChild = this->firstChild;
            //sym->token = this->token;
            sym->token = this->token;//Transfer, so don't take
        }
        this->token = nullptr;

        this->next = nullptr;
        this->prev = nullptr;
        this->firstChild = nullptr;
        this->parent = nullptr;
    }

    void XNode::replaceWith(XNode *const sym) {
        //std::cout << "replaceWith set prev's next to " << sym->first() << ", next's prev to " << sym->last() << "\n";

        //First, update the neighboring nodes
        if(this->next != nullptr) this->next->prev = sym->last();
        if(this->prev != nullptr) this->prev->next = sym->first();

        //Now we alter our own pointers, since they were already used above
        sym->first()->prev = this->prev;
        sym->last()->next = this->next;

        sym->setParent(this->parent, true);
        if(sym->parent != nullptr && sym->parent->firstChild == this) sym->parent->firstChild = sym;//Reassign first child if needed

        this->next = nullptr;
        this->prev = nullptr;
        this->parent = nullptr;


        //unsigned short flg = this->flags;
        //sym->setFlag(flg);
    }

    void XNode::setParent(XNode *const par, bool recursive) {
        this->parent = par;
        if(recursive && this->next != nullptr) this->next->setParent(par, recursive);
    }
    
    bool XNode::canResolve() const {
        if(this->type == Identifier && !this->hasFlag(Resolved | Incomplete) && token != nullptr/* && this->symbol() == nullptr*/) return true;
        else return false;
    }

    XNode* XNode::resolve(VM *const vm) {//Try to resolve all unresolved nodes in our branch
        //bool ret = true;
        if(this->canResolve()) {
            Symbol *ref = Symbol::Reference(this->token->str, vm);
            this->relinquishTo(ref);
            delete this;
            return ref->resolve(vm);
        }

        //if(this->firstChild != nullptr) ret &= firstChild->resolve(vm);
        //if(this->next != nullptr) ret &= next->resolve(vm);

        return this;//ret;
    }

    bool XNode::execute(VM *const vm) {
        bool ret = true;
        if(this->firstChild != nullptr) ret &= firstChild->execute(vm);
        if(this->next != nullptr) ret &= next->execute(vm);

        return ret;
    }

    bool XNode::evaluate(VM *const vm) {
        _unused(vm);
        return true;
    }

    std::string XNode::raw() const {
        if(token != nullptr) {
            //Render non-printing characters nicely for terminals
            if(token->str.size() == 1 && token->str[0] < 32) return "\\u" + std::to_string(int(token->str[0]));
            else return token->str;
        } else return "";
    }
    std::string XNode::str() const {//Recursively calls str() on children/next
        std::stringstream ss;
        ss << this->raw();
        if(this->firstChild != nullptr) ss << "{";
        for(auto child = this->firstChild; child != nullptr; child = child->next) ss << child->str() << (child->next == nullptr? "" : ", ");
        if(this->firstChild != nullptr) ss << "}";
        //if(this->next != nullptr) ss << next->str();

        return ss.str();
    }

    //Symbol* XNode::symbol() const {
    //    return nullptr;
    //}

    XNode* XNode::blankClone() const {
        return new XNode();
    }

    XNode* XNode::clone(bool withChildren, bool withNext) const {//Clones the most derived object (NOTE: MUST be overridden by subclasses)
        //std::cout << "\tclone invoked on " << this->infoString() << " raw='" << this->raw() << "', str='" << this->str() << "'\n";
        if(this->type == Invalid) return nullptr;//Break at EOE

        XNode *ret = this->blankClone();
        ret->mirror(this);

        if(withChildren && firstChild != nullptr) {
            ret->addChild(firstChild->clone(true, true));//Clone all children
            //ret->setParent(this);
        } else ret->firstChild = nullptr;

        if(withNext && next != nullptr) {
            ret->append(this->next->clone(withChildren, true));
        } else ret->next = ret->prev = nullptr;

        ret->parent = nullptr;
        ret->prev = nullptr;
        return ret;
    }
    void XNode::mirror(const XNode *const o) {
        /*this->prev = o->prev;
        this->next = o->next;

        this->parent = o->parent;
        this->firstChild = o->firstChild;*/
        this->token = o->token;
        this->token->take();

        //this->subID = o->subID;
        //this->type = o->type;
        this->rawTypeInfo = o->rawTypeInfo;
        this->symbolPtr = o->symbolPtr;
    }
    bool XNode::matches(const XNode *const o, bool rawMatch) const {
        bool ret = this->type != Invalid;;
        ret &= o->type != Delimiter || this->type == Delimiter;//Delimiter normally won't match anything
        ret &= o->type != Invalid;
        if(rawMatch) ret &= this->raw() == o->raw();
        //Implemented in derived classes
        return ret;
    }

    XNode* XNode::MakePlaceholder(uint8_t sID, uint8_t type, uint16_t flags) {
        XNode * ret = new XNode();
        ret->setType(type);
        ret->setFlag(flags | Placeholder);
        ret->subID = sID;
        return ret;
    }

    XNode* XNode::MakeContainer(uint16_t flags) {
        XNode * ret = new XNode();
        ret->setType(Container);
        ret->setFlag(flags);
        return ret;
    }

    std::string XNode::debugInfo() const {
        std::stringstream ss;
        ss << "#" << this->iden() << " " << FlagString(this->flags) << "\\n";
        if(token != nullptr) ss <<token->lineNumber << ":" << token->offset << " ";
        else ss << "SYN ";
        ss << Dot::Escape(this->infoString()) << "\\n\\'" << Dot::Escape(this->str()) << "\\'";//Dot::Escape(token == nullptr? (this->symbol() == nullptr? "null" : this->symbol()->name()) : this->raw()) << "\\'";
        return ss.str();
    }

    const std::string& XNode::typeName() const {
        if(this->symbol() != nullptr && this->symbol() != this) return this->symbol()->typeName();
        //std::cout << "XNode typestring called for " << this->str() << "\n";
        return TypeString(this->type);
    }

    std::string XNode::iden() const {
#ifdef CASPAR_XNODE_DBG
        return "XN" + std::to_string(this->debugIden);
#else
        return "XN";
#endif
    }

    void XNode::debugGraph(std::fstream& stream, std::stringstream& ss, bool root) {
 #ifdef CASPAR_XNODE_DBG
        if (root) {
            stream << "digraph G {\n";
            stream << "rankdir=TD ;\n";
            stream << "splines=ortho;\n";
            //stream << "compound=true;\n";
            //stream << "\tnodesep=1.0;\n";
            //stream << "\tratio = \"compress\";\n";
            stream << "\tnode [fontsize=14 shape=box];\n";
        }

        //stream << "\tsubgraph cluster_" << this->iden() << " {\n\t\tnode[group=\"g" << this->iden() << "\", rank=same];\n";
        //stream << "\t\tcolor=invis;\n";
        stream << "\t" << this->iden() <<
        " [label=\"" << this->debugInfo()
        << "\"];\n\n";


        if(next != nullptr) {
            next->debugGraph(stream, ss, false);
        }
        //stream << "\t};\n";

        for(XNode *child = firstChild; child != nullptr; child = child->next) {
            stream << "\t" << this->iden() << "->" << child->iden();
            if(child != firstChild) stream << " [style=dotted,dir=none]";
            else stream << " [color=red]";
            stream << ";\n";

            if(child->prev != nullptr) {
                stream <<  "\t" << child->prev->iden() << " -> " << child->iden() << " [color=gray,constraint=false];\n";
            }
            //"[taillabel=\""<<(origin&diff).iden(far()&diff) <<"\", "<<
            //"[headlabel=\"" << " " << "\"];\n";
        }

        if(firstChild != nullptr) {
            //stream << "\tsubgraph cluster_" << this->iden() << " {\n\t\tnode[group=\"gc" << this->iden() << "\"];\n";
           // stream << "\t\tcolor=invis;\n";;
            firstChild->debugGraph(stream, ss, false);
            //stream << "\t};\n";
        }

        //if (root) stream << "\tsubgraph cluster_FWD {\n\t\tedge [dir=none]\n" << ss.str() << "\t};\n}\n";
        if(root) stream << "\n}\n";
#else
        return;
#endif
    }

    inline static std::string _indentTo(unsigned int off) {
        std::stringstream ss;
        for(unsigned int i = 0; i < off; i++) ss << "\t";
        return ss.str();
    }

    void XNode::debugTree() {
        #ifdef CASPAR_XNODE_DBG

        unsigned int indentation = this->depth();
        std::cout << _indentTo(indentation) << "#" << this->index() << "\t" <<this->infoString() << " raw='" << this->raw() << "', this = "<<this<<", parent="<<this->parent<<", child="<<this->firstChild<<", next="<<this->next<<"\n";

        if(firstChild != nullptr) {
            if(firstChild == this) {
                std::cerr << "SELF CHILD LOOP\n";
                return;
            }
            firstChild->debugTree();
            std::cout << _indentTo(indentation) << "#\n";
        }

        if(next != nullptr) {
            next->debugTree();
        }
        #else
        return;
        #endif
    }

    bool XNode::rvalue() const { return  this->symbol() != nullptr && !this->lvalue(); };
    bool XNode::lvalue() const { return  this->symbol() != nullptr && this->symbol()->metadata() != nullptr;; };

    const std::string& XNode::TypeString(const uint8_t t) {
      static const std::string iden = "Identifier", expr = "Expression", st = "Statement", lit = "Literal",
      delim="Delimiter", patt = "Pattern", blk = "Block", cntr = "Container", inval = "Invalid";
        switch(t) {
            case Identifier: return iden;
            case Expression: return expr;
            case Statement: return st;
            case Literal: return lit;
            case Delimiter: return delim;
            case Pattern: return patt;
            case Block: return blk;
            case Container: return cntr;
            default: case Invalid: return inval;
        }
    }

    static inline bool xhasFlag(const unsigned int flags, const unsigned int mask) {
        return (flags & mask) == mask;
    }

    std::string XNode::FlagString(const unsigned int flags) {
        std::stringstream ss;

        if(xhasFlag(flags, Incomplete)) ss << "Incomplete, ";
        else if(xhasFlag(flags, Resolved)) ss << "Resolved, ";
        else if(xhasFlag(flags, Partial)) ss << "Partial, ";
        else ss << "Unresolved, ";

        if(xhasFlag(flags, Mutable)) ss << "Mutable, ";
        if(xhasFlag(flags, Trait)) ss << "Trait, ";
        if(xhasFlag(flags, Function)) ss << "Function, ";
        if(xhasFlag(flags, Type)) ss << "Type, ";

        if(xhasFlag(flags, Proxy)) ss << "Proxy, ";
        if(xhasFlag(flags, Scoped)) ss << "Scoped, ";

        if(xhasFlag(flags, Placeholder)) ss << "Placeholder, ";
        if(xhasFlag(flags, Wildcard)) ss << "Wildcard, ";

        if(xhasFlag(flags, Stale)) ss << "Stale, ";

        const std::string ret = ss.str();
        return ret.size() > 2? ret.substr(0, ret.size()-2) : ret;
    }
};
