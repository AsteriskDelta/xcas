#ifndef INC_CASPAR_SCOPE_H
#define INC_CASPAR_SCOPE_H
#include "iCaspar.h"
#include <unordered_map>
#include "Symbol.h"

namespace Caspar {
    class Scope {
    public:
        Scope(Scope *par = nullptr);
        virtual ~Scope();

        Scope* push();
        Scope* pop();

        bool addSymbol(std::string name, Symbol *const symbol);
        Symbol *resolve(const std::string& name);

        inline virtual VM* vm() {
          if(parent == nullptr) return nullptr;
          return parent->vm();
        }
    protected:
        std::unordered_map<std::string, Symbol*> symbols;

        Scope *parent, *child;

        Symbol *localResolve(const std::string& name);
    private:

    };
};

#endif
