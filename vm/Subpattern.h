#ifndef INC_CASPAR_SUBPATTERN_H
#define INC_CASPAR_SUBPATTERN_H
#include "iCaspar.h"
#include <set>
#include "Pattern.h"

namespace Caspar {
    class VM;
    class Pattern;
    struct PatternPtrLess {
        inline bool operator()(Pattern *const a, Pattern *const b) const {
            return a->priority > b->priority;//Negative values are VM-defined, so they take precedence
        }
    };
    
    inline bool PatternPtrLessFn(Pattern *const a, Pattern *const b) {
        return a->priority > b->priority;//Negative values are VM-defined, so they take precedence
    }
   
    
    class SubPattern {
    public:
        SubPattern(const std::string& txt = "");
        ~SubPattern();
        
        std::string pattern;
        
        std::set<Pattern*, PatternPtrLess> matches;
        
        void registerPattern(Pattern *const pattern);
        void unregisterPattern(Pattern *const pattern);
        
        typedef decltype(matches)::iterator iterator;
    protected:
        
    };
    
};


#endif
