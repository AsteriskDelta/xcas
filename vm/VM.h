#ifndef INC_CASPAR_VM_H
#define INC_CASPAR_VM_H
#include "iCaspar.h"
#include "Scope.h"
#include "ParserStream.h"
#include "Subpattern.h"
#include "Pattern.h"
#include <queue>
#include <set>
#include <deque>
#include <unordered_map>
#include "XNode.h"
#include "Block.h"

namespace Caspar {
    class VM : public Block {
    public:
        VM(VM *const par);
        virtual ~VM();

        void setInputStream(ParserStream *stream);
        void setOutputStream(std::ostream *outStream);

        bool process(const TokenList& tokens);

        bool transform(XNode *& root, Pattern::Order from, Pattern::Order to, int bestPriority = -2147000);

        bool link(XNode *& root);//Resolves as many symbols as possible

        bool execute(XNode *& root);

        bool step();//Execute the next sequence in the pipeline, returns false if pipeline is empty

        void addPattern(class Pattern *const pattern);
        void removePattern(class Pattern *const pattern);

        void addSubPattern(XNode *sub, class Pattern *pattern, Pattern::Order ord);
        void removeSubPattern(XNode *sub, class Pattern *pattern, Pattern::Order ord);

        Block *scope;//Represents the most derived scope at the VMs present state
        std::vector<Block*> scopeStack;
        void pushScope(Block *scope);
        bool popScope();

        void use();

        inline virtual VM* vm() override {
          return this;
        }

        static thread_local VM *active;
        void debugXNodeTree(XNode *root);

        static Symbol* Immediate;

        inline ParserStream* stream() const {
            return streamPtr;
        }
    protected:
        std::queue<XNode*> pipeline;

        std::set<class Pattern*, PatternPtrLess> patterns;
        std::deque<std::unordered_map<std::string, SubPattern>> subPatterns;

        std::ostream* output;

        ParserStream *streamPtr;
    };
};

#endif
