#include "Scope.h"

namespace Caspar {
    Scope::Scope(Scope *par) : symbols(), parent(par), child(nullptr) {
        
    }
    Scope::~Scope() {
        if(this->child != nullptr) delete this->child;
    }
    
    Scope* Scope::push() {
        if(this->child != nullptr) {
            std::cerr << "Attempt to push() new scope over existing, child scope. Bailing out\n";
            return nullptr;
        }
        return (this->child = new Scope(this));
    }
    Scope* Scope::pop() {
        if(this->parent == nullptr) {
            std::cerr << "Attempt to pop root VM, wtf man?\n";
            return nullptr;
        }
        
        Scope *const par = this->parent;
        delete this;
        return par;
    }
    
    bool Scope::addSymbol(std::string name, Symbol *const sym) {
        if(symbols.find(name) != symbols.end()) {
            std::cerr << "Scope trying to overwrite existing local variable " << name << ", leaking memory at " << sym << "\n";
            return false;
        }
        
        std::cout << "SCOPE add sym " << name << " @ " << sym->infoString() << "\n";
        symbols[name] = sym;
        return true;
    }
    
    Symbol* Scope::localResolve(const std::string& name) {
        auto it = symbols.find(name);
        if(it == symbols.end()) return nullptr;
        else return it->second;
    }
    
    Symbol* Scope::resolve(const std::string& name) {
        Symbol *ret = this->localResolve(name);
        std::cout << "\tattempting to resolve '" << name << "'\n";
        if(ret == nullptr && parent != nullptr) ret = parent->resolve(name);
        
        if(ret != nullptr) {
            std::cout << "RESOLVE: " << name << " -> " << ret->infoString() << "\n";
        }
        
        return ret;
    }
    
    /*void Scope::addPatterns(const std::vector<Pattern*>& patterns) {
        
    }*/
};
