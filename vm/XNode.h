#ifndef INC_CASPAR_XNODE_H
#define INC_CASPAR_XNODE_H
#include "iCaspar.h"
#include "ParserStream.h"
#include <sstream>

#define CASPAR_XNODE_DBG

namespace Caspar {
    class VM;
    struct XNode_Root {//For polymorphic template deduction

    };

    class Symbol;

    class XNode : public XNode_Root {
    public:
        enum Type : uint8_t {//Up to 16 types
            Invalid,
            Identifier,
            Expression,
            Statement,
            //Operator,//Same as function, for now
            Literal,
            Delimiter,
            Pattern,
            Block,
            Container
        };
        enum Flags : uint16_t {//ONLY 12 bits!!!
            Unresolved  = 0b0,
            Resolved    = 0b1,
            Partial     = 0b10,
            Incomplete  = Partial | Resolved,

            Mutable     = 0b100,
            Trait       = 0b1000,
            Function    = 0b10000,
            Type        = 0b100000,

            Proxy       = 0b1000000,//If we're the stored symbol, or simply a reference to it
            Scoped      = 0b10000000,//If we have our own sub-scope relative to surrounding nodes

            Placeholder = 0b100000000,//A placeholder in a pattern
            Wildcard    = 0b1000000000,//May be applied any number of times
            Stale       = 0b10000000000,
            Evaluated   = 0b100000000000,
            Masked      = 0b1000000000000,
            EndFlags
        };

        XNode();
        virtual ~XNode();

        void setType(uint16_t newType);

        union {
            struct {
                unsigned int flags : 16;
                unsigned int subID : 8;
                unsigned int type : 4;
                unsigned short padding : 4;//This'll be useful later, not that we have a choice...
            };
            unsigned int rawTypeInfo;
        };

        XNode *parent, *prev, *next, *firstChild;

        Symbol *symbolPtr;
        Token *token;

        inline virtual Symbol* symbol() const {
            return symbolPtr;
        };
        inline virtual XNode *node() const {
            return const_cast<XNode*>(this);
        }

        virtual XNode* operator[](const unsigned int idx);
        inline XNode* at(const unsigned int idx) {
            return (this->operator[](idx));
        };
        unsigned int size() const;
        unsigned int index() const;
        unsigned int depth() const;

        void unlinkTokens();

        XNode* root() const;
        XNode* first() const;
        XNode* last() const;

        XNode* children();
        inline bool hasChildren() const {
            return this->firstChild != nullptr;
        }

        //Iterates through children, then next, all the way through the tree
        struct iterator {
            XNode *current;
            inline iterator(XNode *n) : current(n) {
            }

            inline operator XNode *const& () const {
                return current;
            }
            inline operator XNode *& () {
                return current;
            }

            inline iterator& operator++() {
                if(current->firstChild != nullptr) {
                    current = current->firstChild;
                } else if(current->next != nullptr) {
                    current = current->next;
                }  else {//Recurse upwards until we find another node to traverse
                    while(current != nullptr && current->next == nullptr) current = current->parent;
                    if(current != nullptr) current = current->next;
                }
                return *this;
            }
            inline operator bool() const {
                return current != nullptr;
            }
            inline XNode* operator->() const {
                return current;
            }
        };

        struct bu_iterator {
            XNode *current;
            inline bu_iterator(XNode *n) {
                    XNode *lChild = n;
                    while(lChild->firstChild != nullptr) lChild = lChild->firstChild;
                    current = lChild;
            }

            inline operator XNode *const& () const {
                return current;
            }
            inline operator XNode *& () {
                return current;
            }

            inline bu_iterator& operator++() {
                if(current->next != nullptr) {
                    current = current->next;
                    //Skip to the lowest-order child
                    while(current->firstChild != nullptr) current = current->firstChild;
                } else if(current->parent != nullptr) {
                    current = current->parent;
                } else current = nullptr;
                return *this;
            }
            inline operator bool() const {
                return current != nullptr;
            }
            inline XNode* operator->() const {
                return current;
            }
        };

        inline iterator begin() {
            return iterator{this};
        }

        inline bu_iterator bottomUp() {
            return bu_iterator{this};
        }

        inline bool ripe() const {
            for(auto it = firstChild; it != nullptr; it = it->next) if(!it->hasFlag(Stale)) return false;
            return true;
        }

        inline void freshen() {
            this->clearFlag(Stale);
            if(this->next != nullptr) next->freshen();
            if(this->firstChild != nullptr) firstChild->freshen();
        }
        inline void spoil() {
            this->setFlag(Stale);
            if(this->next != nullptr) next->spoil();
            if(this->firstChild != nullptr) firstChild->spoil();
        }

        void addChild(XNode *const child);
        void append(XNode *const node);

        void relinquishTo(XNode *const sym);//Replaces 'this' with the sym WITHOUT its children, preparing this for deletion
        void replaceWith(XNode *const sym);//Replaces 'this' with the sym AND its children, preparing this for deletion
        void setParent(XNode *const par, bool recursive = false);//Replaces 'this' with the sym, preparing for deletion

        virtual bool canResolve() const;
        virtual XNode* resolve(VM *const vm);//Try to resolve all unresolved nodes in our branch

        virtual bool execute(VM *const vm);
        virtual bool evaluate(VM *const vm);

        virtual std::string raw() const;
        virtual std::string str() const;//Recursively calls str() on children/next

        //virtual Symbol* symbol() const;

        virtual XNode *blankClone() const;//Just creates the (most derived) object (NOTE: MUST be overridden by subclasses)
        virtual XNode* clone(bool withChildren, bool withNext) const;//Clones the most derived object
        virtual void mirror(const XNode *const o);// mirror all characteristics of the other XNode (NOTE: MUST be overridden by subclasses)
        virtual bool matches(const XNode *const o, bool rawMatch = false) const;

        void debugGraph(std::fstream& stream, std::stringstream& ss, bool root = true);
        void debugTree();

        static const std::string& TypeString(const uint8_t t);
        virtual const std::string& typeName() const;

        static std::string FlagString(const unsigned int flags);
        inline std::string flagString() const {
            return FlagString(this->flags);
        }

        inline virtual std::string infoString() const {
            return this->typeName() + (this->hasFlag(Placeholder)? std::to_string(int(this->subID)) :"") + "{"+this->flagString()+"}";
        }

        inline void setFlag(const decltype(rawTypeInfo)&& flag) {
            flags |= flag;
        }
        inline void clearFlag(const decltype(rawTypeInfo)&& flag) {
            flags &= ~flag;
        }
        inline bool hasFlag(const decltype(rawTypeInfo)&& flag) const {
            return (flags & flag) == flag;
        }

        inline void setSubID(const uint8_t i) {
            subID = i;
        }

        bool rvalue() const;
        bool lvalue() const;
        inline bool evalue() const { return  this->symbol() != nullptr; };
        inline bool ivalue() const { return this->symbol() == nullptr; };//non-evaluable

        template<typename T>
        inline T* as() {
            return dynamic_cast<T*>(this);
        }

        static XNode* MakePlaceholder(uint8_t sID, uint8_t type, uint16_t flags);

        static XNode* MakeContainer(uint16_t flags = 0x0);
    protected:
        std::string debugInfo() const;
        std::string iden() const;

#ifdef CASPAR_XNODE_DBG
        unsigned int debugIden;
#endif
    };
};


#endif
