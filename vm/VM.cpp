#include "VM.h"
#include "XNode.h"
#include <fstream>
#include <cstdlib>

namespace Caspar {
    thread_local VM *VM::active = nullptr;
    Symbol* VM::Immediate = nullptr;

    VM::VM(VM *const par) : Block(), scope() {
        this->Scope::parent = par;
        this->pushScope(this);
    }
    VM::~VM() {

    }

    void VM::addPattern(class Pattern *const pattern) {
        std::cout << "Added pattern \"" << pattern->str() << "\"\n";
        //sleep(1);
        patterns.insert(pattern);
    }

    void VM::removePattern(class Pattern *const pattern) {
        patterns.erase(pattern);
    }

    void VM::addSubPattern(XNode *sub, class Pattern *pattern, Pattern::Order ord) {
        if(sub->raw().empty()) return;//Don't emplace global patterns

        while(ord >= subPatterns.size()) subPatterns.push_back(std::unordered_map<std::string, SubPattern>());

        auto& map = subPatterns[ord];
        auto it = map.find(sub->raw());

        if(it == map.end()) map[sub->raw()] = SubPattern(sub->raw());
        map[sub->raw()].registerPattern(pattern);
    }
    void VM::removeSubPattern(XNode *sub, class Pattern *pattern, Pattern::Order ord) {
        if(ord > subPatterns.size()) subPatterns.resize(static_cast<unsigned int>(ord));

        auto& map = subPatterns[ord];
        map[sub->raw()].unregisterPattern(pattern);
    }

    void VM::pushScope(Block *blk) {
        scopeStack.push_back(blk);
        this->scope = blk;
    }
    bool VM::popScope() {
        if(scopeStack.empty()){
            std::cerr << "Scope overflow, can't pop global scope!" << std::endl;
            return false;
        }

        this->scope = scopeStack.back();
        scopeStack.pop_back();
        return true;
    }

    bool VM::process(const TokenList& tokens) {
        XNode *ret = nullptr;//new XNode();
        //ret->setType(XNode::Statement);

        for(auto *xtoken : tokens) {
            if((xtoken->type & ~Token::Discardable) == 0x0) continue;

            std::cout << "EMIT\t\t" << xtoken->lineNumber << ":" << xtoken->offset << "\t" << xtoken->type << "\t\t" << xtoken->str << "\n";
            XNode *childNode = new XNode();
            childNode->token = xtoken;
            childNode->token->take();

            if((xtoken->type & Token::Literals) != 0x0) childNode->setType(XNode::Literal);
            else if((xtoken->type & Token::Identifiers) != 0x0) childNode->setType(XNode::Identifier);
            else if((xtoken->type & Token::Signifiers) != 0x0) childNode->setType(XNode::Delimiter);
            else childNode->setType(XNode::Invalid);

            //ret->addChild(childNode);
            if(ret == nullptr) ret = childNode;
            else ret->append(childNode);
        }

        if(ret == nullptr) return false;
        //if(!ret->hasChildren()) return false;

        this->debugXNodeTree(ret);

        pipeline.push(ret);
        return true;
    }

    bool VM::transform(XNode *& root, Pattern::Order from, Pattern::Order to, int bestPriority) {
        //Obsolete - pattern matching is now recursive
        //if(root->firstChild != nullptr) this->transform(root->firstChild, from, to);

        //Iterate patterns until we've parsed all given data
        bool patternApplied = true, patternFound = false;
        auto& map = subPatterns[from];

        //std::cout << "Pre-transform:\n";
        //root->debugTree();

        struct Candidate {
            class Pattern *pattern = nullptr;
            XNode *node = nullptr;
            int score = -2147000;
        };
        Candidate best;
        
        constexpr bool debugTransform = false;

        while(patternApplied) {
            patternApplied = false;
            best = Candidate();
            best.score = bestPriority;
            //std::cout << "\tbeginning iterative pattern search...\n";
            for(auto node = root->begin(); node != nullptr; ++node) {
                if(patternApplied) break;

                if(debugTransform) std::cout << "\t\ttrying to find a pattern match for "<<node->infoString()<<"='" << node->raw() << "'\n";
                //if(node->type != XNode::Identifier) continue;
                //sleep(1);
                auto subIt = map.find(node->raw());
                if(subIt == map.end()) continue;
                if(debugTransform) std::cout << "\t\tFound pattern matching/containing subpattern '" << node->raw() << "'\n";

                auto& patternSet = subIt->second.matches;
                for(auto it = patternSet.begin(); it != patternSet.end(); ++it) {
                    class Pattern *const pattern = *it;
                    if(pattern->match(node, from)) {
                        int score = pattern->priority;
                        if(debugTransform) std::cout << "\t\tpatt " << pattern->str() << " matched with score=" << score << ", best.score = " << best.score <<"\n";
                        if(score >= best.score) {
                            if(debugTransform) std::cout <<"\tset new best pattern with score "<<score <<" >= " << best.score <<" for \"" << pattern->str() <<"\"\n";
                            best.pattern = pattern;
                            best.node = node;
                            best.score = score;
                        }
                    } else {
                        std::cout << "\tFailed to match \n";
                    }
                }
            }

            if(best.pattern != nullptr) {
                if(debugTransform) std::cout << "chose pattern \"" << best.pattern->str() <<"\" to \"" << best.pattern->to->str()<<"\"\n";
                std::cout << "Applying " << best.pattern->str() << " to graph...\n";
                best.pattern->apply(best.node, from, to);
                patternApplied = true;
                patternFound = true;
                //Always keep the root pointer valid, as the original objects were deleted/appropriated by the pattern application
                //if(node->prev == nullptr && node->parent == nullptr) root = node;

                //std::cout << "Matched and applied, debug graph:\n";
                //best.node->debugTree();
                //this->debugXNodeTree(best.node);
                //root = best.node;//->root();//->first();//Reset to beginning to match newly created patterns
                //std::cout << "root graph:\n";
                break;
            }
        }

        //std::cout << "Writing final xnode tree, post-transformation:\n";
        //this->debugXNodeTree(root);

        return patternFound;
    }

    bool VM::link(XNode *& root) {//Resolves as many symbols as possible
        //Now that all patterns have been applied, resolve as many unresolved symbols as we can
        return root->resolve(this);
    }

    bool VM::execute(XNode *& root) {
        bool nodeExecuted = false;
        //Node-by-node, execute left-to-right the XNodes (linearly, as their execute() functions recurse to their children)
        for(auto it = root->root()->bottomUp(); bool(it); ++it) {
            Symbol *sym = it->symbol();
            //Always attempt to resolve and execute first-order children (statements)
            /*if(it->type == XNode::Identifier && !it->hasFlag(XNode::Stale) &&
                !it->hasFlag(XNode::Resolved) && it->parent == nullptr) {
                std::cout << "RESOLVE top-level node " << it->infoString() << "\n";
                it.current = it->resolve(this);
                sym = it->symbol();
            }*/
            if(sym == nullptr || it->hasFlag(XNode::Stale)
            || !it->hasFlag(XNode::Resolved)/* || !it->ripe()*/) continue;

            XNode *resetRoot = it->root();
            it->setFlag(XNode::Stale);
            bool reset = sym->executeOn(it, this);
            if(reset) it = resetRoot;

            nodeExecuted = true;

            std::cout << "After execution of instr:\n";
            //this->debugXNodeTree(it->root());
            //it->root()->debugTree();
            //sleep(1);

            break;
        }
        //return root->execute(this);
        return nodeExecuted;
    }

    bool VM::step() {
        if(pipeline.empty()) return false;

        XNode *root = pipeline.front(), *node = pipeline.front();
        pipeline.pop();
        //auto from = Pattern::Input, to = Pattern::Internal;
        //auto& map = subPatterns[from];
        bool changed = true;
        while(changed) {
            changed = false;
            
            int bestPriority = -2147000;
            for(auto it = root->root()->begin(); bool(it); ++it) {
                node = it;
                Symbol *sym = it->symbol();
                if(!(sym == nullptr || it->hasFlag(XNode::Stale)
                    || !it->hasFlag(XNode::Resolved))) {
                    bestPriority = std::max(bestPriority, sym->executionPriority());
                std::cout << "\t\t\t" << it->str() << " priority = " << sym->executionPriority() << "\n";
                }
            }
            
            std::cout << "Best execution score: " << bestPriority << "\n";
            
            if(this->transform(root, Pattern::Input,Pattern::Internal, bestPriority)) changed = true;
            
            std::cout << "Changed? " << changed << "\n";
            
            //If we haven't found anything to do yet, try resolving a symbol (L->R)
            if(!changed && bestPriority < -5000) {
                for(auto it = root->root()->begin(); bool(it); ++it) {
                    if(it->canResolve() && it->resolve(this) != nullptr) {
                        changed = true;
                        break;
                    }
                };
            }

            if(!changed) {
                for(auto it = root->root()->begin(); bool(it); ++it) {
                    node = it;
                    Symbol *sym = it->symbol();
                    if(!(sym == nullptr || it->hasFlag(XNode::Stale)
                    || !it->hasFlag(XNode::Resolved)) && sym->executionPriority() == bestPriority) {
                        XNode *resetRoot = it->root();
                        it->setFlag(XNode::Stale);
                        bool reset = sym->executeOn(it, this);
                        if(reset) it = root = resetRoot->bottomUp();
                        
                        //std::cout << "After execution of instr:\n";
                        //this->debugXNodeTree(it->root());
                        //it->root()->debugTree();
                        changed = true;
                        break;
                    }
                }
            }
            
            
            
            if(changed) {
                this->debugXNodeTree(root);
                root->debugTree();
            }
        }

        //Add the node for posterity of future passes through our block/potential function
        this->scope->addNode(root);

        return true;
    }

    /*
    bool VM::step() {
        if(pipeline.empty()) return false;

        XNode *root = pipeline.front(), *node = pipeline.front();
        pipeline.pop();
        auto from = Pattern::Input, to = Pattern::Internal;
        auto& map = subPatterns[from];
        struct Candidate {
            Pattern *pattern = nullptr;
            XNode *node = nullptr;
            int score = -2147000;
        };
        Candidate best;

        //this->link(node);
        bool dirty = true;
        while(dirty) {
            dirty = false;
            best = Candidate();

            for(auto it = root->root()->bottomUp(); bool(it); ++it) {
                node = it;
                Symbol *sym = it->symbol();
                if(!(sym == nullptr || it->hasFlag(XNode::Stale)
                || !it->hasFlag(XNode::Resolved))) {
                    XNode *resetRoot = it->root();
                    it->setFlag(XNode::Stale);
                    bool reset = sym->executeOn(it, this);
                    if(reset) it = resetRoot;
                    dirty = true;
                    std::cout << "After execution of instr:\n";
                    this->debugXNodeTree(it->root());
                    it->root()->debugTree();
                    break;
                } else {
                    std::cout << "\t\ttrying to find a pattern match for "<<node->infoString()<<"='" << node->raw() << "'\n";
                    auto subIt = map.find(node->raw());
                    if(subIt == map.end()) continue;
                    std::cout << "\t\tFound pattern matching/containing subpattern '" << node->raw() << "'\n";

                    auto& patternSet = subIt->second.matches;
                    bool patternApplied = false;

                    for(auto it2 = patternSet.begin(); it2 != patternSet.end(); ++it2) {
                        Pattern *const pattern = *it2;

                        if(pattern->match(node, from)) {
                            int score = pattern->priority;
                            if(score >= best.score) {
                                std::cout <<"\tset new best pattern with score "<<score <<" >= " << best.score <<"\n";
                                best.pattern = pattern;
                                best.node = node;
                                best.score = score;
                            }
                        } else {
                            std::cout << "\tFailed to match \n";
                        }
                    }


        std::cout << "Executing...\n";
        this->execute(node);
        //node->unlinkxtokens();
        std::cout << "After execution:\n";
        this->debugXNodeTree(node);
        node->debugTree();

                }
                //sleep(1);
                if(best.pattern != nullptr) {
                    std::cout << "chose pattern \"" << best.pattern->from->str() <<"\"\n";
                    best.pattern->apply(best.node, from, to);
                    //Always keep the root pointer valid, as the original objects were deleted/appropriated by the pattern application
                    //if(node->prev == nullptr && node->parent == nullptr) root = node;

                    std::cout << "Matched and applied, debug graph:\n";
                    best.node->debugTree();
                    this->debugXNodeTree(best.node);
                    root = best.node;//->root();//->first();//Reset to beginning to match newly created patterns
                    //std::cout << "root graph:\n";
                }
            }


            //std::cout << "Transforming...\n";
            //dirty |= this->transform(node, Pattern::Input, Pattern::Internal);

            //std::cout << "Executing...\n";
            //if(!dirty) dirty |= this->execute(node);
            //std::cout << "After execution:\n";
            //this->debugXNodeTree(node);
            //node->debugTree();

            //std::cout << "After transform:\n";
            //this->debugXNodeTree(node);
            //sleep(1);
        };

        if(false && output != nullptr) {
            //XNode *outNode = node->clone();
            this->transform(node, Pattern::Internal, Pattern::Output);
            //this->debugXNodeTree(node);
            (*output) << node->str() << "\n";
        }

        return true;
    }*/

    void VM::use() {
        VM::active = this;
    }

    void VM::setOutputStream(std::ostream *outStream) {
        this->output = outStream;
    }

    void VM::setInputStream(ParserStream *newStream) {
        this->streamPtr = newStream;
    }

    void VM::debugXNodeTree(XNode *root) {
        return;
        static unsigned int dbgID = 0;
        std::stringstream ss;
        ss << "dbg/xnodes" << std::setw(3) << std::setfill('0') << (dbgID++) << ".dot";
        const std::string dbgFile = ss.str();

        //std::cout << "Writing debug info to " << dbgFile << "\n";
        std::fstream fs(dbgFile, std::fstream::out | std::fstream::trunc);
        std::stringstream links;
        root->debugGraph(fs, links);
        fs.close();

        std::string cmd = "dot -Gdpi=50 -Tpng "+dbgFile+" -o " + dbgFile + ".png";
        system(cmd.c_str());

        std::cout << "Debug info written to " << dbgFile << "\n";
    }
};
