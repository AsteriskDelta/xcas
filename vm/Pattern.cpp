#include "Pattern.h"
#include "VM.h"
#include "XNode.h"
#include "Symbol.h"
#include "String.h"

namespace Caspar {
    Pattern::Pattern() : id(0), priority(0), from(nullptr), to(nullptr), write(nullptr) {

    }
    Pattern::~Pattern() {
        if(from != nullptr) delete from;
        if(to != nullptr) delete to;
        if(write != nullptr) delete write;
    }

    XNode* Pattern::blankClone() const {
        return new Pattern();
    };
    void Pattern::mirror(const XNode *const o) {
        Pattern *op = const_cast<Pattern*>(dynamic_cast<const class Pattern*>(o));
        //std::cout << "PATTERN::mirror invoked on " << op << " -> " << this << " ("<<op->from<<", " <<op->to<<", "<<op->write<<")\n";
        if(op == nullptr) std::cerr << "Pattern::mirror called with non-pattern arguement\n";

        if(op->from != nullptr) this->setFrom(op->from->clone(true,true));
        if(op->to != nullptr) this->setTo(op->to->clone(true,true));
        if(op->write != nullptr) this->setWrite(op->write->clone(true,true));
        XNode::mirror(o);
    }
    bool Pattern::matches(XNode* o, bool rawMatch) const {
        //std::cout << "Pattern::matches " << o->symbol()->typeName() << "\n";
        return XNode::matches(o, rawMatch) && this->typeName() == o->symbol()->typeName();//this->matches(o, Order::Input);
    }

    const std::string& Pattern::typeName() const{
        static const std::string tName = "pattern";
        return tName;
    }

    std::string Pattern::str() const {
        std::stringstream ss;
        ss << "`";
        for(auto it = this->from; it != nullptr; it = it->next) {
            if(it->hasFlag(XNode::Placeholder)) {
                if(it->hasFlag(XNode::Wildcard)) ss << "$$" << it->subID;
                else ss << "$" << it->subID;
            } else {
                ss << it->str();
            }
        }
        ss << "` -> `";
        for(auto it = this->to; it != nullptr; it = it->next) {
            if(it->hasFlag(XNode::Placeholder)) {
                if(it->hasFlag(XNode::Wildcard)) ss << "$$" << it->subID;
                else ss << "$" << it->subID;
            } else {
                ss << it->str();
            }
        }
        ss << "`";
        return ss.str();
    }

    //Registers any requisite subpatterns automatically
    void Pattern::setFrom(XNode *node) {
        this->from = node;//this->parseRawStream(node);
    }
    void Pattern::setTo(XNode *node) {
        this->to = node;//this->parseRawStream(node);
    }
    void Pattern::setWrite(XNode *node) {
        this->write = node;//this->parseRawStream(node);
    }

    void Pattern::registerTo(Scope * tScope) {
        VM *const targetVM = tScope->vm();
        this->vm = targetVM;

        vm->addPattern(this);

        std::cout << "Registering pattern to VM:\n";

        for(uint8_t ord = Order::Input; ord < Order::EndOrders; ++ord) {
            if(orders[ord] == nullptr) continue;
            std::cout << "order: " << int(ord) << " has " << orders[ord]->size() << " nodes\n";
            orders[ord]->debugTree();

            bool first = true;
            for(XNode *node = orders[ord]; node != nullptr; node = node->next) {
                //std::cout << "\t\t" << node->infoString() << "\t'" << node->raw() << "'\n";
                if(!node->hasFlag(XNode::Placeholder)) {
                    if(!first) continue; //If we didn't match the first required subpattern, don't bother to match any others

                    firstSubpatternOffset[ord] = node->index();
                    vm->addSubPattern(node, this, (Order)ord);
                    first = false;
                }
            }
        }
    }
    void Pattern::unregister() {
        for(uint8_t ord = Order::Input; ord < Order::EndOrders; ++ord) {
            for(XNode *node = orders[ord]; node != nullptr; node = node->next) {
                if(!node->hasFlag(XNode::Placeholder)) vm->removeSubPattern(node, this, (Order)ord);
            }
        }

        vm->removePattern(this);
    }

    int Pattern::match(XNode *stream, Order ord) {
        return this->apply(stream, ord, ord, true);
    }

    int Pattern::apply(XNode *& stream, Order ordFrom, Order ordTo, bool dryRun) {
        if(stream == nullptr) return 0;
        const XNode *patternFrom = orders[ordFrom];
        const XNode *patternTo = orders[ordTo];
        XNode *streamEdge = stream;

        constexpr bool dbgApply = false;

        //Allow wildcards before first literal subpattern, so this gets a little iterative...
        while(streamEdge != nullptr && !patternFrom->matches(streamEdge)) {
            if(dbgApply) std::cout << "PRE-BACKTRACK \"" << patternFrom->str()<<"\"" << patternFrom->infoString() << " FAILS to match "
            << "\"" << streamEdge->str() << "\"" << streamEdge->infoString() << "\n";
            streamEdge = streamEdge->prev;
        }
        if(streamEdge == nullptr) {
            if(dbgApply) std::cout << "Failed to find any matches while backtracking\n";
            return 0;//No matches anywhere, bail out
        }

        //We match now, so be greedy and take as many matching nodes as possible
        while(streamEdge != nullptr && patternFrom->matches(streamEdge)) {
            if(dbgApply) std::cout << "BACKTRACK \"" << patternFrom->str()<<"\"" << patternFrom->infoString() << " matches "
            << "\"" << streamEdge->str() << "\"" << streamEdge->infoString() << "\n";
            streamEdge = streamEdge->prev;
        }
        if(streamEdge == nullptr) streamEdge = stream->first();//Matched all nodes from the beginning, so start there
        else streamEdge = streamEdge->next;//Didn't match, so the previous was our starting point

        XNode *matchStart = streamEdge, *matchEnd = streamEdge, *resultNode = nullptr;
        const XNode *fromEdge = patternFrom;

        if(!dryRun) resultNode = patternTo->clone(true, true);
        std::cout << "resultNode = " << resultNode << "\n";
        std::cout << "Applying pattern: "  << this->str() << "\n";

        //Next, find the end of the matched pattern
        while(fromEdge != nullptr) {
            XNode *submatchStart = streamEdge, *submatchEnd = streamEdge;
            bool matches;
            while(submatchEnd != nullptr && (matches = fromEdge->matches(submatchEnd) ) ) {
                if(dbgApply) std::cout << "\"" << fromEdge->str()<<"\"" << fromEdge->infoString() << " matches "
                << "\"" << submatchEnd->str() << "\"" << submatchEnd->infoString() << "\n";
                if(fromEdge->next != nullptr && fromEdge->next->matches(submatchEnd)) {
                    if(submatchEnd != nullptr && submatchEnd != submatchStart) submatchEnd = submatchEnd->prev;//Skip the terminating literal on change of pattern

                    break;
                }

                submatchEnd = submatchEnd->next;

                if(!fromEdge->hasFlag(XNode::Wildcard)) {
                    matches = false;
                    break;//Match one and only one node, so don't continue
                }
            }
            if(submatchEnd != nullptr && !matches) {
                if(dbgApply) std::cout << fromEdge->infoString() << " \"" << fromEdge->raw() << "\" does NOT match " << submatchEnd->infoString() << "  \"" << submatchEnd->raw() << "\"\n";
                if(submatchEnd == submatchStart) return 0;

                if(submatchEnd->prev == nullptr) {
                    std::cerr << "BAD Something went terribly wrong with submatching code...\n";
                } else submatchEnd = submatchEnd->prev;

            } else if(submatchEnd == nullptr) {
                submatchEnd = submatchStart->last();//Matched everything, so mark the last node
                //if(!matches && submatchEnd == submatchStart) return false;
            }
            
            std::cout << "post matching cycle, matched=" << matches << ", dryRun=" << dryRun << "\n";
            if(!dryRun && fromEdge->hasFlag(XNode::Placeholder)) {
                std::vector<XNode*> replacements;
                XNode *submatchNext = submatchEnd->next;
                submatchEnd->next = nullptr;//Null it so we can clone without overshooting

                //Replace all the placeholder nodes in resultNode with our submatch
                for(auto it = resultNode->begin(); bool(it); ++it) {
                    if(!it->hasFlag(XNode::Placeholder) || it->hasFlag(XNode::Stale)) continue;
                    else if(it->subID != fromEdge->subID) continue;//It's a different placeholder, skip it

                    it->clearFlag(XNode::Placeholder);//We're about to fill it, so clear the flag
                    if(dbgApply) std::cout << "cloning from " << submatchStart << " to " << submatchEnd << "\n";
                    XNode *newChild = submatchStart->clone(true,true);
                    //it->addChild(newChild);
                    //newChild->flags |= it->flags;

                    if(dbgApply) {
                        std::cout << "splice-replace node at " << it.current << ", prev=" << it->prev << ", next=" << it->next << "\n";
                        std::cout << "newChild = " << newChild << ", newRoot = " << newChild->root() << "\n";
                    }
                    //Cull the placeholder node and replace it with its value
                    it->replaceWith(newChild);
                    delete it.current;
                    resultNode = it.current = newChild->root();//Restart after any change to the tree's topology (otherwise we skip stuff)
                    //newChild->root()->debugTree();
                    
                    replacements.push_back(newChild);
                    newChild->spoil();
                }
                
                submatchEnd->next = submatchNext;
                for(XNode *xn : replacements) xn->freshen();
            }

            fromEdge = fromEdge->next;
            streamEdge = submatchEnd->next;
            if(streamEdge == nullptr) break;//We ran out of things to match, so break out
        }

        if(/*streamEdge == nullptr && */fromEdge != nullptr &&
            (!fromEdge->hasFlag(XNode::Wildcard) || fromEdge->next != nullptr)) {
                std::cout << "invalid fromEdge=" << fromEdge << ", next=" << fromEdge->next << "\n";
                return 0;//There's another node that was never matched
            }

        //matchEnd is now the last node that matches the pattern
        if(streamEdge == nullptr) matchEnd = matchStart->last();
        else matchEnd = streamEdge->prev;

        //If we made it here, then resultNode now contains a complete version of patternTo with placeholders replaced
        if(!dryRun) {
            XNode *rawMatch = matchStart;

            //Splice in our result
            XNode *leftHand = matchStart->prev, *rightHand = matchEnd->next;
            resultNode->prev = leftHand;
            resultNode->next = rightHand;
            resultNode->parent = matchStart->parent;
            if(matchStart->parent != nullptr && matchStart->parent->firstChild == matchStart) matchStart->parent->firstChild = resultNode;

            if(leftHand != nullptr) leftHand->next = resultNode;
            if(rightHand != nullptr) rightHand->prev = resultNode;

            //And free the now-detached nodes
            matchEnd->next = nullptr;
            matchStart->prev = nullptr;
            //delete rawMatch;

            stream = resultNode->root();//Redirect the stream to the (possibly) new root node for future passes

            //Handle immediate execution (for raw strings, comments, etc.)
            if(resultNode->symbol() != nullptr && resultNode->symbol()->hasProperty(VM::Immediate)) {
                resultNode->symbol()->executeOn(resultNode, VM::active);
            }
        }


        return 1;
    };

    void Pattern::propertyAssigned(Symbol *sym, XNode *node) {
        std::cout << "PATTERN PROP ASSIGN "<<sym->infoString()<<"@" << sym << " -> " << node->infoString() << "@"<<node << "\n";
        Pattern *pattern = dynamic_cast<Pattern*>(node);
        std::cout << "\tgot pattern ptr " << pattern << "\n";
        //VM::active->addPattern(pattern);

        //Create the nested structure needed for parse tree creation
        XNode *oldTo = pattern->to;
        pattern->to = Symbol::BlankReference(vm);//new Symbol();
        pattern->to->symbolPtr = sym;
        pattern->to->setFlag(XNode::Resolved);
        if(oldTo != nullptr) pattern->to->addChild(oldTo);

        std::cout << "from patt:\n";
        pattern->from->debugTree();
        VM::active->debugXNodeTree(pattern->from);
        std::cout << "to patt:\n";
        pattern->to->debugTree();
        VM::active->debugXNodeTree(pattern->to);
        pattern->registerTo(VM::active);
    }

    /*int Pattern::apply(XNode *& stream, Order ordFrom, Order ordTo, bool dryRun) {
        if(stream == nullptr) return 0;
        const XNode *src = orders[ordFrom];
        const XNode *dest = orders[ordTo];
        XNode *cur = stream;

        //Allow wildcards before first literal subpattern, so this gets a little recursive...
        while(cur != nullptr && !src->matches(cur)) cur = cur->prev;//return this->apply(cur->prev, ordFrom, ordTo, dryRun);
        if(cur == nullptr) return 0;//No matches anywhere, bail out

        //Be greedy, so take as many matching nodes as possible
        while(cur != nullptr && src->matches(cur)) cur = cur->prev;
        if(cur == nullptr) cur = stream->first();//Matched all nodes from the beginning, so start there
        else cur = cur->next;//Didn't match, so the previous was our starting point

        //chain of xnodes to be spliced in, fitting the new pattern (spliceEdge)
        XNode *spliceChain = nullptr;//dryRun? nullptr : dest->clone(true, true);
        const XNode *edge;
        int matchCnt = 0;

        if(!dryRun) {
            std::cout << "cloning output node\n";
            spliceChain = dest->clone(true, true);
            spliceChain->debugTree();
            std::cout << "end clone\n";
        }

        std::vector<XNode*> toDelete;

        //Save the previous node to link to, since stream's object will be moved into the splice chain
        //if(!dryRun) spliceChain->first()->prev = cur->prev;

        for(edge = src; edge != nullptr; edge = edge->next) {
            XNode *matchStart = nullptr, *matchEnd = nullptr;
            bool endCurrentEdge = !edge->hasFlag(XNode::Wildcard);

            while(cur != nullptr) {
                std::cout << "\t\t\tattempting to match '" << cur->raw() << "' with '" << edge->raw() << "' = " << int(edge->matches(cur)) << "\n";
                if(!edge->matches(cur)) break;

                //If we're a wildcard, stop when we hit the first match of another type
                if(matchStart != nullptr && edge->hasFlag(XNode::Wildcard) && edge->next != nullptr && edge->next->matches(cur)) {
                    endCurrentEdge = true;
                    break;
                }

                matchCnt++;
                if(matchStart == nullptr) matchStart = cur;
                matchEnd = cur;//->next;
                cur = cur->next;

                if(endCurrentEdge) break;

                /\*if(!dryRun && edge->hasFlag(XNode::Placeholder)) {//Insert the matched node under the proper placeholder
                    const auto subID = edge->subID;
                    //TODO: In addition to first-order, iterate through dest's children (requires a custom iterator class for spliceEdge)
                    for(XNode *spliceEdge = spliceChain; spliceEdge != nullptr; spliceEdge = spliceEdge->next) {
                        if(!spliceEdge->hasFlag(XNode::Placeholder) || spliceEdge->subID != subID) continue;

                        //Replace the placeholder with the matched node
                        spliceEdge->relinquishTo(cur);
                        if(spliceEdge == spliceChain) spliceChain = cur;//Make sure our spliceChain ptr remains valid
                        delete spliceEdge;

                        spliceEdge = cur;//Cur was removed from the stream and passed in here
                        std::cout << "Spliced stream output: " << "" << "\n";
                        spliceEdge->debugTree()
                    }*\/
            }

                //Cur was moved into our splice chain with updated adjacency ptrs, so use the saved matchEnd ptr
                //cur = matchEnd;

            //Couldn't match anything, bail out
            if(matchStart == nullptr) {
                break;
            } else if(!dryRun) {
                auto nodeAfterSplice = matchEnd->next;
                //Cut the matched nodes off from the rest of the graph

                //if(matchStart->prev != nullptr && matchStart->prev->next != nullptr) matchStart->prev->next = nullptr;
                //if(matchEnd->next != nullptr && matchEnd->next->prev != nullptr) matchEnd->next->prev = nullptr;

                matchEnd->next = nullptr;
                matchStart->prev = nullptr;
                /\*if(matchStart->parent != nullptr && matchStart->parent->firstChild == matchStart) {
                    matchStart->parent->firstChild = nullptr;
                    matchStart->parent = nullptr;
                }*\/
                std::cout << "match begins at " << matchStart->infoString() << " and ends at " << matchEnd->infoString() << " to " << spliceChain->infoString() << " from " << edge->infoString() << "\n";

                //if(spliceChain->hasFlag(XNode::Placeholder)) {
                    //for(XNode *spliceEdge = spliceChain; spliceEdge != nullptr; spliceEdge = spliceEdge->next) {
                    for(XNode::iterator spliceEdge(spliceChain); bool(spliceEdge); ++spliceEdge) {
                        //if(spliceEdge.current == spliceChain) continue;//Don't splice directly into the root node (it can't be a placeholder)
                        const auto subID = edge->hasFlag(XNode::Placeholder)? edge->subID : 0xFF;
                        if(!spliceEdge->hasFlag(XNode::Placeholder) || spliceEdge->subID != subID) continue;
                        std::cout << "splicing into " << spliceEdge->infoString() << "\n";
                        //Update the placeholder with the matched node(s)
                        XNode *spliceSource = matchStart->clone(true, true);//Since it's cut off, just clone it
                        spliceEdge->addChild(spliceSource);
                    }
                //}


                std::cout << "Spliced stream output: " << "" << "\n";
                spliceChain->root()->debugTree();
                delete matchStart;//Decoupled from everything, so just delete it
            }
        }

        //Didn't match anything/enough, break out
        if(matchCnt == 0) {
            if(!dryRun && matchCnt != 0) {
                std::cerr << "Pattern::apply() called on stream that didn't match, destroying the instruction stream... good luck\n";
            }
            return 0;
        }

        if(!dryRun) {
            //spliceChain->last()->next = cur;//Cur was first node that didn't match
            //Update the reference for our caller
            stream = spliceChain->root();
        }

        return matchCnt;
    }*/

    XNode* Pattern::parseRawStream(XNode *stream) {
        /*if(stream->raw() != "`") {
            std::cerr << "Pattern parse request invoked on stream that doesn't start with backtick ( got "<< stream->raw() << " instead)!\n";
            return nullptr;
        }*/

        XNode *ret = nullptr;
        bool escaped = false;
        uint8_t placeholderID = 0;

        if(stream->raw() == "`") stream = stream->next;

        for(XNode *edge = stream; edge != nullptr; edge = edge->next) {//Skip the starting backtick
            const std::string raw = edge->raw();
            XNode *xn = nullptr;

            if(false && raw == "\\" && !escaped) {//TODO: Re-enable escape sequences based on next token contents
                escaped = true;
                continue;//Skip the escape token
            } else escaped = false;

            if(!escaped) {
                if(raw == "$$" || raw == "$") {
                    if(edge->next == nullptr) {
                        std::cerr << "Pattern match token given, but no XNodes follow\n";
                        continue;
                    }

                    Symbol *ref = Symbol::Reference(edge->next->raw(), vm);
                    ref->setFlag(XNode::Placeholder | (raw.size() > 1? XNode::Wildcard : 0x0));
                    ref->setSubID(placeholderID++);

                    xn = ref;
                } else if(raw == "`") {
                    break;
                }
            }

            if(xn == nullptr) {
                Types::String *lit = new Types::String(raw);
                xn = lit;
            }

            if(ret == nullptr) ret = xn;
            else ret->append(xn);
        }

        return ret;
    }
};
