#ifndef INC_CASPAR_SYMBOL_H
#define INC_CASPAR_SYMBOL_H
#include "iCaspar.h"
#include "XNode.h"

namespace Caspar {
    class VM;
    class Pattern;

    struct SymbolMetadata {
        std::string name;
        Scope *scope = nullptr;
        std::string abbrv, symbol;
        XNode* xnode = nullptr;
    };

    class Symbol : public XNode {
    public:
        Symbol();
        virtual ~Symbol();

        const std::string& name() const;
        void name(const std::string& newName, Scope *tScope);//Automatically registers in scope

        void assign(XNode *node);
        void assign(Symbol *sym);

        std::unordered_map<Symbol*, XNode*> properties;
        void addProperty(XNode *node);
        bool hasProperty(Symbol *sym);
        virtual void propertyAssigned(Symbol *owner, XNode *node);

        inline virtual std::string infoString() const override {
            return XNode::infoString() + "->" + this->name();// + this->name();
        }

        //Overridden to avoid dynamic_cast in performance critical code
        inline virtual class Pattern* getPattern() {
            return nullptr;
        }

        //Disabled to avoid ambiguity
        /*inline Symbol& operator=(XNode *const& ptr) {
            this->assign(ptr);
            return *this;
        }*/

        Scope* scope() const;

        virtual bool executeOn(XNode *node, VM *const vm);

        virtual XNode* resolve(VM *const vm) override;//Try to resolve all unresolved nodes in our branch

        virtual bool execute(VM *const vm) override;

        virtual std::string str() const override;//Recursively calls str() on children/next
        virtual std::string raw() const override;

        virtual XNode *blankClone() const override;//Just creates the (most derived) object (NOTE: MUST be overridden by subclasses)
        virtual void mirror(const XNode *const o) override;// mirror all characteristics of the other XNode (NOTE: MUST be overridden by subclasses)
        virtual bool matches(const XNode *const o, bool rawMatch = false) const override;
        /*
        inline bool rvalue() const { return  state.real && !state.named; };
        inline bool lvalue() const { return  state.real &&  state.named; };
        inline bool evalue() const { return  state.real; };
        inline bool ivalue() const { return !state.real; };//non-evaluable
        */
        inline SymbolMetadata* metadata() const {
            /*if(!this->lvalue()) return nullptr;
            else */return this->metaPtr;//reinterpret_cast<SymbolMetadata*>(rawState & (~0b11));
        }
        virtual Symbol* symbol() const {
            if(this->symbolPtr == nullptr || this->symbolPtr == this) return const_cast<Symbol*>(this);
            else return this->symbolPtr->symbol();
        };
        /*inline XNode* xnode() const {
            //if(!state.real) return nullptr;
            //else if(!state.named) return reinterpret_cast<XNode*>(rawState & (~0b11));
            //else return this->metadata()->xnode;
            if(this->symbol() != nullptr) return static_cast<XNode*>(this->symbol());
            else return static_cast<XNode*>(const_cast<Symbol*>(this));
        }
        inline Symbol* symbol() const {
            //if((this->rvalue() || this->lvalue()) && this->hasFlag(XNode::Proxy | XNode::Resolved)) return reinterpret_cast<Symbol*>(rawState & (~0b11));
            //else return nullptr;
            if(this->lvalue()) return const_cast<Symbol*>(this);
            else if(this->rvalue()) return symPtr;
            else return nullptr;
        }*/
        
        virtual int  executionPriority() const;
        virtual void executionPriority(int16_t val);

        using XNode::as;

        static Symbol* Reference(const std::string& raw, VM *const vm);
        static Symbol* Reference(Symbol *const sym, VM *const vm);
        inline static Symbol* BlankReference(VM *const vm) {
            return Reference("",vm);
        }
    protected:
        //union {
            /*struct {
                bool named : 1;
                bool real : 1;
                unsigned long long ptrBase : 62;
            } state;*/
            //uint64_t rawState;
        //};
        union {
            SymbolMetadata *metaPtr;
            XNode *xnodePtr;
        };
        int16_t executionPriority_;
    };
};

#endif
