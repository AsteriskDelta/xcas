#include "Subpattern.h"

namespace Caspar {
    SubPattern::SubPattern(const std::string& txt) : pattern(txt), matches() {
        
    }
    SubPattern::~SubPattern() {
        
    }
    
    void SubPattern::registerPattern(Pattern *const patt) {
        matches.insert(patt);
    }
    
    void SubPattern::unregisterPattern(Pattern *const patt) {
        matches.erase(patt);
    }
};
