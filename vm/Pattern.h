#ifndef INC_CASPAR_PATTERN_H
#define INC_CASPAR_PATTERN_H
#include "iCaspar.h"
#include "XNode.h"
#include <list>
#include <functional>
#include "Type.h"

namespace Caspar {
    class VM;
    class Subpattern;

    class Pattern : public Type {
    public:
        Pattern();
        virtual ~Pattern();

        enum Order : uint8_t {
            Input = 0,
            Internal = 1,
            Output = 2,
            EndOrders
        };

        enum Flags : unsigned int {
            DescendScope    = 0b1,
            AscendScope     = 0b10,
            IndentScope     = 0b100,//Increase the scope without overwriting it, creating if it doesn't exist
            ResetScope      = 0b1000,//End of line, but doesn't descend/delete unless the scope is overwritten
            Override        = 0b10000,//Don't parse any deeper into this node, we'll process it manually (promise)
            EndFlags
        };

        typedef int Idt_t;

        //Registers any requisite subpatterns automatically
        void setFrom(XNode *node);
        void setTo(XNode *node);
        void setWrite(XNode *node);

        //void registerTo(VM *const targetVM);
        void unregister();

        virtual bool matches(XNode *node, bool rawMatch = false) const;

        virtual const std::string& typeName() const override;
        virtual std::string str() const override;

        virtual XNode *blankClone() const override;//Just creates the (most derived) object (NOTE: MUST be overridden by subclasses)
        virtual void mirror(const XNode *const o) override;// mirror all characteristics of the other XNode (NOTE: MUST be overridden by subclasses)

        void registerTo(Scope *tScope);
        int match(XNode *stream, Order ord);//From input syntax
        //Called ONLY after verifying that match() (dryRun=true) returns true, since this modifies the stream in-place
        int apply(XNode *& stream, Order from, Order to, bool dryRun = false);

        inline virtual Pattern* getPattern() override {
            return this;
        }

        Idt_t id;
        int priority;
        union {
            struct {
                XNode *from, *to, *write;
            };
            XNode *orders[3];
        };

        virtual void propertyAssigned(Symbol *sym, XNode *node) override;
    protected:
        VM *vm;
        int firstSubpatternOffset[3];

        XNode *parseRawStream(XNode *stream);//Allocates new stream with the parsed data
    };
};

#endif
