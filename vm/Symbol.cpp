#include "Symbol.h"
#include "VM.h"
#include "Scope.h"

namespace Caspar {
    Symbol::Symbol() : XNode(), executionPriority_(0) {
        this->metaPtr = nullptr;
    }
    Symbol::~Symbol() {
        if(this->lvalue() && this->metadata() != nullptr) delete this->metadata();
    }

    const std::string& Symbol::name() const {
        static const std::string Unnamed = "";
        /*if(this->ivalue()) return Unnamed;
        else if(this->rvalue() && !this->hasFlag(XNode::Proxy)) return Unnamed;
        else if(this->rvalue()) return this->symbol()->name();
        */
        if(this->lvalue() && this->metadata() != nullptr) return this->metadata()->name;
        else if(this->symbol() != nullptr && this->symbol() != this) return this->symbol()->name();
        else return Unnamed;
    }
    void Symbol::name(const std::string& newName, Scope *tScope) {//Automatically registers in scope
        SymbolMetadata *meta = new SymbolMetadata();
        //this->rawState = reinterpret_cast<uint64_t>(meta);
        metaPtr = meta;
        //state.real = state.named = true;
        this->setFlag(XNode::Resolved);
        //this->token = nullptr;

        meta->name = newName;
        meta->scope = tScope;
        tScope->addSymbol(newName, this);
    }

    void Symbol::assign(XNode *node) {
        //this->rawState = reinterpret_cast<uint64_t>(node);
        this->xnodePtr = node;

        if(node != nullptr) {
            //state.real = true;
            this->setFlag(XNode::Resolved);
        } else {
            this->clearFlag(XNode::Resolved);
            this->setFlag(XNode::Incomplete);
            //state.real = false;
        }

        //state.named = false;
    }

    void Symbol::assign(Symbol *sym) {
        //Since they do the same checks, re-use for brevity
        //this->assign(reinterpret_cast<XNode*>(sym));
        //this->symPtr = sym;
        //state.real = sym != nullptr;
        //state.named = false;
        this->symbolPtr = sym;
        //this->setFlag((sym != nullptr? XNode::Resolved : 0x0));
        if(sym != nullptr) {
            this->setFlag(XNode::Resolved);
            this->setType(XNode::Identifier);
        }
        //if(sym != this && sym != nullptr) this->setFlag(XNode::Proxy);
    }
    /*
    inline Symbol& Symbol::operator=(XNode *const& ptr) {
        this->assign(ptr);
        return *this;
    }*/

    void Symbol::addProperty(XNode *node) {
        Symbol *sym = node->symbol();
        if(sym == nullptr) {
            node = node->resolve(VM::active);
            sym = node->symbol();
        }
        
        if(sym == nullptr) {
            std::cerr << "attempted to add null property " << node->infoString() << " of type " << node->typeName() << ", str=" << node->str() << "\n";
            return;
        }

        auto it = properties.find(sym);
        if(it == properties.end()) {
            properties[sym] = new XNode();
        } else {
            it->second->addChild(node);//append(node);
        }
        std::cout << "adding property " << this->infoString() << " to " << sym->infoString() << " on " << node->infoString()  << "\n";
        sym->propertyAssigned(this, node);
    }

    bool Symbol::hasProperty(Symbol *sym) {
        auto it = properties.find(sym);
        return it != properties.end();
    }

    void Symbol::propertyAssigned(Symbol *owner, XNode *node) {
        std::cout << "Symbol::propertyAssigned " << this->infoString() << "@" << this << " node " << node->infoString() << "@"<<node<<"\n";
    }

    Scope* Symbol::scope() const {
        if(this->lvalue()) return this->metadata()->scope;
        else return nullptr;
    }

    XNode* Symbol::resolve(VM *const vm) {//Try to resolve all unresolved nodes in our branch
        //bool ret = true;
        std::cout << "Symbol::resolve \"" << this->str() << "\" " << this->infoString() << "\n";
        //sleep(1);
        if(this->symbolPtr != nullptr) {
            this->setFlag(XNode::Resolved);
            this->type = XNode::Identifier;
        }
        if(this->type == XNode::Identifier && this->ivalue()) this->assign(vm->scope->Scope::resolve(this->raw()));

        this->token->release();
        this->token = nullptr;
        //if(this->firstChild != nullptr) ret &= firstChild->resolve(vm);
        //if(this->next != nullptr) ret &= next->resolve(vm);

        return this;//ret;
    }

    bool Symbol::execute(VM *const vm) {
        bool ret = true;

        if(this->firstChild != nullptr) ret &= firstChild->execute(vm);

        if(this->symbol() != nullptr) ret &= this->symbol()->executeOn(this, vm);

        if(this->next != nullptr) ret &= next->execute(vm);

        return ret;
    }

    bool Symbol::executeOn(XNode *sym, VM *const vm) {
        _unused(sym,vm);
        std::cout << "SYM EXEC-ON " << this->infoString() << " ("<<sym->infoString()<<")\n";
        return false;
    }

    std::string Symbol::raw() const {
        /*if(this->symbol() != nullptr) return this->symbol()->raw() + XNode::raw();
        else */return XNode::raw();
    }
    std::string Symbol::str() const {//Recursively calls str() on children/next
        /*if(this->symbol() != nullptr) return this->symbol()->str() + XNode::str();
        else */return XNode::str();
    }

    XNode *Symbol::blankClone() const {
        return new Symbol();
    };
    void Symbol::mirror(const XNode *const o) {
      /*if(o->symbol()->lvalue()) {
          std::cerr << "Symbol::mirror called on lvalue object " << o->symbol()->infoString() << " '" << o->symbol()->raw() << "' -> \"" << o->symbol()->str() << "\"!\n";
          //return;
      }*/
      if(o->symbol() != nullptr) {
          //  this->rawState = o->symbol()->rawState;
          //o->symbol()->symbolPtr = this->symbolPtr;
          //o->symbol()->state = this->state;
          //this->state = o->symbol()->state;
      }
      XNode::mirror(o);
    }
    bool Symbol::matches(const XNode *const o, bool rawMatch) const {
        if(this->symbol() == o->symbol()) return true;
        else if(this->symbol() == this) return XNode::matches(o, rawMatch);// && o->symbol()->rawState == this->rawState;
        else return this->symbol()->matches(o, rawMatch);
    }
    
    int Symbol::executionPriority() const {
        return executionPriority_;
    }
    
    void Symbol::executionPriority(int16_t val) {
        this->executionPriority_ = val;
    }

    //TODO: Allow incomplete resolutions to store metadata as to the desired name
    Symbol* Symbol::Reference(const std::string& raw, VM *const vm) {
        Symbol *ret = new Symbol();
        ret->setType(XNode::Identifier);
        if(!raw.empty()) ret->assign(vm->scope->Scope::resolve(raw));
        return ret;
    }
    Symbol* Symbol::Reference(Symbol *const sym, VM *const vm) {
        Symbol *ret = new Symbol();
        ret->setType(XNode::Identifier);
        ret->assign(sym);
        return ret;
    }
};
