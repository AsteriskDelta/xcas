//#include "iCaspar.h"
#include "Caspar.h"
#include "BufferedParser.h"
//#include "Patterns.h"
//#include "VM.h"
#include <fstream>

#include "Bootstrap.h"

using namespace Caspar;

int main(int argc, char** argv) {
    _unused(argc, argv);
    /*std::string fileName = "patterns01.txt";
    std::cout << "parsing " << fileName << "\n";

    Patterns::LoadFrom(fileName);
    */
    //Line test1("let X be the set of all x in Z+ such that x < 10");
    ParserStream::Init();
    BufferedParser parser;
    /*
    std::string line = "integer: a number x such that (x % 1.0) = 0\n";
    parser.write(line);
    std::cout << "parsing " << line << " for the following tokens:\n";

    auto tokens = parser.readTokens();
    for(auto &token : tokens) {
        std::cout << "\t" << token.lineNumber << ":" << token.offset << "\t" << token.type << "\t\t" << token.str << "\n";
    }
    */

    std::fstream outFS("out.tex", std::fstream::out | std::fstream::trunc);

    std::cout << "\n\nStarting CASPAR VM:\n";
    VM *caspar = Caspar::CreateVM();
    caspar->use();
    Caspar::BootstrapVM(caspar);

    caspar->setInputStream(&parser);
    caspar->setOutputStream(&outFS);

    std::cout << "\n\nVM Created, processing tokens...\n";

    std::ifstream fs("standard/operators.casp");
    std::string line;
    unsigned int lineCnt = 0, lineLimit = 15;
    while(std::getline(fs, line) && fs.good()) {
        std::cout << line << "\n";
        parser.write(line + "\n");

        auto tokens = parser.getStatement();
        while(!tokens.empty()) {
            for(auto &token : tokens) {
                std::cout << "\t" << token->lineNumber << ":" << token->offset << "\t" << token->type << "\t\t" << token->str << "\n";
            }

            std::cout << "Processing...\n";
            caspar->process(tokens);
            std::cout << "Stepping...\n";
            while(caspar->step());

            tokens = parser.getStatement();
        }
        lineCnt++;
        if(lineCnt >= lineLimit) break;
    }

    //

    delete caspar;

    outFS.close();

    return 0;
};
