NPN : Transistor, {
    collector   : pin 1
    base        : pin 2
    emitter     : pin 3
}

Diode : {
    anode: pin 1
    cathode: pin 2
}

LED<Color> : Diode, {
    annotate Color
}

Analog.OpAmp : {
    vMinus:
    vPlus:
    vOut:
}

Logic.Or : {
    input1: public
    input2: public
    input1 -> input2
}

Logic.Not : {
    input: public
    output: public
}

Logic.Not #NPN : {
    gnd, vSrc: inherit
    transistor: NPN
    transistor.emitter <- gnd
    transistor.collector <- vSrc
    input: public, 4.7kOhm -> transistor.base
    output: public, transistor.collector
}

Logic.And #NPN : {
    gnd, vSrc: inhert
    not1, not2: Logic.Not
    or: Logic.Or

    input1: public
    input2: public
    output: public, or.output

    not1.input <- input1
    not2.input <- input2
    or.input1 <- not1.output
    or.input2 <- not2.output
}

Logic.Nand #Composite : {
    andGate: Logic.And
    inv: Logic.Not

    input1 : public, pin 1
    input2 : public, pin 2
    bequeath input1, input2 to andGate

    inv.vIn <- andGate.vOut

    vOut : public, pin 3 <- inv.vOut
}

LM331 : PDIP8, {
    cOut        : pin 1
    cRef        : pin 2
    fOut        : pin 3
    gnd         : pin 4
    rcFilter    : pin 5
    threshold   : pin 6
    comparator  : pin 7
    vSrc        : pin 8
}

MCP4091 : PDIP8, {
    vSrc        : pin 1
    cs          : pin 2
    sck         : pin 3
    mosi        : pin 4
    latch       : pin 5
    vRef        : pin 6
    gnd         : pin 7
    vOut        : pin 8
}

DAC #b12: MCP4091, {

}


gnd:
vPower:
vLogic:

Converters.VtoF #Simple : LM331, {
    gnd, vSrc: inherit

    cRef <- 12kOhm resistor <- 5kOhm pot <- gnd

    decouple comparator 0.1uF
    decouple rcFilter 0.01uF
    decouple vSrc 0.47uF

    vPower -> 6.8kOhm resistor -> rcFilter

    cThreshold: cOut -> threshold -> (0.1uF mylar, 100kOhm)
    pulldown cThreshold 47Ohm
    pullup cThreshold 22kOhm pot

    public vIn: comparator -> 100kOhm resistor
    public fOut
    public vLogic: fOut -> 10kOhm resistor
}

Analog.Adder: {
    public vIn1:
    public vIn2:
    public vOut:
}
Analog.OpAmp: {
    public vPlus:
    public vMinus:
    public vOut:
}

Analog.Subtractor: <gain := 1>, {
    inherit vSrc, gnd
    public input1:
    public input2:

    var r1,r2,r3,r4
    constrain r3 / r1 == r4 / r2
    constrain r2 / r1 == gain
    constrain 47 < r1,r2,r3,r4
    minimize r1,r2,r3,r4

    amp: Analog.OpAmp
    amp.vPlus -> r4 Ohms -> gnd
    amp.vPlus <- r3 Ohms <- input2
    amp.vMinus <- r1 Ohms <- input1
    amp.vOut -> r2 Ohms -> amp.vMinus

    public output: amp.vOut
}

Analog.Summer: <gain1 := 1,gain2 := 1>, {
    inherit gnd, vSrc
    public input1, input2

    var resistanceNumer = Math.LCM(gain1, gain2)
    var resistanceDenom = Math.Max(gain1, gain2)
    var r1 = gain2 * resistanceNumer / resitanceDenom, r2 = gain1*resistanceNumer / resistanceDenom;

    amp: Analog.OpAmp
    amp.vPlus <- r1 kOhm <- input1
    amp.vPlus <- r2 kOhm <- input2
    amp.vPlus <- resistanceNumer kOhm <- gnd

    amp.vMinus <- resistanceNumer kOhm <- amp.vOut

    public vOut: amp.vOut
}

Analog.Integrator : <factor := 1>, {
    gnd, vSrc: inherit
    input: public

    var RC = -factor
    var R, C
    constrain R * C == RC
    constrain R > 1000
    minimize R

    amp: Analog.OpAmp
    amp.vMinus <- R kOhm <- input
    amp.vMinus <- C Farads <- amp.vOut
    amp.vPlus <- ground

    reset: public
    pulldown reset


    switch : Transistor #NPN
    switch.base <- reset
    switch.collector <- amp.vOut
    switch.emitter -> amp.vMinus

    output: public, amp.vOut
}

NerveSys.Compositor: {
    vLogic:inherit

    mosi, latch, sck, vSrc, gnd, cs, select: public

    bequeath gnd, vSrc, latch, sck, mosi

    //Logic.Not dacSelect
    selectCS: Transistor#NPN
    selectEnable: Transistor#NPN
    selectCS.base <- select
    selectCS.collector <- mosi
    selectMosi: private, selectCS.emitter

    //dacSelect.vIn <- select
    //dacSelect.vOut -> selectCS.base

    freqDac1, freqDac2, ampDac1, ampDac2: MCP4091
    baseDac: MCP4091

    freq1, freq2: Converters.VtoF
    freq1.vIn <- freqDac1.vOut
    freq2.vIn <- freqDac2.vOut

    waveAdder, baseAdder: Analog.Adder
    waveMult1, waveMult2: Analog.Multiplier

    waveMult1.vIn1 <- freq1.vOut
    waveMult2.vIn1 <- freq2.vOut

    waveMult1.vIn2 <- ampDac1
    waveMult2.vIn2 <- ampDac2

    waveAdder.vIn1 <- waveMult1.vOut
    waveAdder.vIn2 <- waveMult2.vOut

    baseAdder.vIn1 <- waveAdder.vOut
    baseAdder.vIn2 <- baseDac.vOut

    public waveform: baseAdder.vOut

    selector: CD4021
    clear selector.vIn
    selector.vIn <- selectEnable.emitter
    selectEnable.collector <- vLogic
    selectEnable.base <- cs

    clear selector.mosi
    selector.mosi <- selectMosi
}

Voltage.Converter #30V: {
    gnd: inherit
    vIn: public
    vOut: public
}

IndicatorLED<voltage, Color> : LED<Color>: {
    led: LED
    anode: public
    cathode: public

    var r1 = voltage / 5.5 * 4.7kOhm

    anode -> r1 -> led.anode
    led.cathode -> cathode
}

NerveSys.PowerSupply: {
    input12V: public
    gnd: public

    vReg1, vReg2: Voltage.Regulator #5V
    vReg3: Voltage.Regulator #3.3V

    ind1: IndicatorLED<12, Red>
    ind2: IndicatorLED<5.5, Blue>
    ind3: IndicatorLED<3.3, Green>

    decouple input12V 0.1uF
    decouple input12V 0.47uF

    vConv1: Voltage.Converter #30V

    input12V -> (ind1.anode, vReg1.vIn, vReg2.vIn, vReg3.vIn)
    gnd <- (ind1.cathode, ind2.cathode, ind3.cathode)

    vReg1.vOut <- vReg2.vOut
    ind2.anode <- vReg1.vOut
    ind3.anode <- vReg3.vOut

    output12V: public, input12V
    output5V: public, vReg1.vOut
    output3V: public, vReg3.vOut
}
