#include "BufferedParser.h"

namespace Caspar {
    BufferedParser::BufferedParser() : ParserStream(), buffer(DefaultSize), bufferName(), lineCnt(0) {

    }
    BufferedParser::~BufferedParser() {

    }

    bool BufferedParser::open(const std::string& path) {
        if(!ParserStream::open(path)) return false;

        bufferName = path;
        return true;
    }
    //virtual void close() override;

    bool BufferedParser::write(const std::string& data) {
        if(static_cast<unsigned int>(buffer.available()) < data.size()) return false;

        buffer.push(data.c_str(), data.size());
        return true;
    }

    std::list<Token*> BufferedParser::readTokens(Token::Type types, const std::string extraDelimiters) {
        decltype(buffer)::iterator eol = buffer.begin();
        std::string tmp;//TODO: make this point to a part of the cyclic buffer, this is inefficient af
        tmp.reserve(buffer.size());

        auto it = buffer.begin();
        while(it && *it == '\n') {
            lineCnt++;
            ++it;
        }

        for(; it != buffer.end(); ++it) {
            if(*it == '\n') {
                eol = it;
                break;
            }
            tmp.push_back(*it);
        }

        if(eol == buffer.begin()) return std::list<Token*>();

        else buffer.edge.trailing = it.idx;//std::min(buffer.end().idx, int(eol.idx + tmp.size()));//Update trailing edge, having consumed so many characters plus the newline

        return ParserStream::TokenizeLine(tmp, types, extraDelimiters, lineCnt);
    }

    bool BufferedParser::completed() const {
        return false;
    }
    bool BufferedParser::isEmpty() const {
        return buffer.empty();
    }
};
