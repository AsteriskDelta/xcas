#include "ParserFileStream.h"

namespace Caspar {
    ParserFileStream::ParserFileStream() :  BufferedParser() {

    }
    ParserFileStream::~ParserFileStream() {

    }

    bool ParserFileStream::open(const std::string& path) {
        if(!BufferedParser::open(path)) return false;

        file.open(path, std::fstream::in);
        if(!file) return false;

        std::string line = "";
        while(std::getline(file, line) && file.good()) {
            BufferedParser::write(line + "\n");
        }

        return true;
    }
    void ParserFileStream::close() {
        file.close();
    }

    std::list<Token*> ParserFileStream::readTokens(Token::Type types, const std::string extraDelimiters) {
        //We already buffered the entire file, just execute the buffer
        return BufferedParser::readTokens(types, extraDelimiters);
    }

    bool ParserFileStream::completed() const {
        return BufferedParser::isEmpty();
    }
};
