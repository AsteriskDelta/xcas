#ifndef INC_PARSERFSTREAM_H
#define INC_PARSERFSTREAM_H
#include "iCaspar.h"
#include "BufferedParser.h"
#include <fstream>

namespace Caspar {
    class ParserFileStream : public BufferedParser {
    public:
        ParserFileStream();
        virtual ~ParserFileStream();

        virtual bool open(const std::string& path) override;
        virtual void close() override;

        virtual std::list<Token*> readTokens(Token::Type types = Token::All, const std::string extraDelimiters = "") override;

        //Return  empty line when reading file, otherwise ALWAYS skip empty  lines and return the next valid (non-empty) one
        //virtual const std::string& readLine() override;

        virtual bool completed() const override;
    protected:
        std::fstream file;
        std::string filePath;
    };
};


#endif
