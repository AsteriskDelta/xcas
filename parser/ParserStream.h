#ifndef INC_PARSERSTREAM_H
#define INC_PARSERSTREAM_H
#include "iCaspar.h"
#include <functional>
#include <list>

/*  What are tokens?
 * - variable names (starting with a character, allowing numbers after, no special characters except _
 * - special characters (ie +, ", ', @, etc.)
 * - No spaces or tabs in tokens, save for scope indentation (although they split tokens)
 * - numbers
 * */

namespace Caspar {

    struct Token {
        enum Type : unsigned int {
            Invalid     = 0b0,
            Delimiter   = 0b1,
            Symbol      = 0b10,
            Number      = 0b100,
            Special     = 0b1000,
            String      = 0b10000,
            Raw         = 0b100000,
            Indent      = 0b1000000,
            EOE         = 0b10000000,
            Intrin      = 0b100000000,
            SOE         = 0b1000000000,
            Negate      = 0b10000000000,
            Last        = Negate,
            All         = Delimiter | Symbol | Number | Special | String | Raw | Indent | EOE | SOE | Intrin,
            Discardable = Invalid | Delimiter,
            Literals    = Number | String,
            Identifiers = Symbol | Special | Intrin,
            Signifiers  = SOE | EOE
        };

        Type type;
        std::string str;
        int offset = 0;
        unsigned int lineNumber = 0;

        int referenceCount = 0;
        inline void take() {
            if(this == nullptr) return;
            this->referenceCount++;
        }
        inline void release() {
            if(this == nullptr) return;
            this->referenceCount--;
            if(this->referenceCount <= 0) delete this;
        }
    };
    
    typedef std::list<Token*> TokenList;

    class ParserStream {
    public:

        ParserStream(); // flags, globalDelimiters
        virtual ~ParserStream();

        virtual bool open(const std::string& path);

        virtual void close();// int x = 5 "default"

        //Returns the name of the stream (file path, console path, etc.)
        virtual const std::string& name() const = delete;

        virtual std::list<Token*> readTokens(Token::Type types = Token::All, const std::string extraDelimiters = "");
        virtual std::list<Token*> getStatement(Token::Type types = Token::All, const std::string extraDelimiters = "");

        void push(ParserStream *stream);
        void pop();

        virtual bool completed() const;
        //virtual std::list<Token> readTokens(Token::Type types = Token::All, const std::string extraDelimiters = "") = delete;

        //No longer needed
        //Return a token in string form, returning empty when the end of line is reached
        //Didn't see point of helper functions in terms of code readability/modularity
        //virtual Token readToken();

        //What line are we reading from?
        //virtual unsigned int lineNumber() const;

        //How many chars into the line was the last token read at?
        //virtual unsigned short tokenOffset() const;

        static TokenList TokenizeLine(std::string line, Token::Type types = Token::All, const std::string& extraDelimiters = "", unsigned int lineID = 0);

        Token::Type tokenFlags;//Set to Token::All by default
        std::string globalDelimiters;//By default, " \t" (whitespace)

        ParserStream *parent, *child;

        static void Init();
    protected:
        //Return  empty line when reading file, otherwise ALWAYS skip empty  lines and return the next valid (non-empty) one -> put in currentLine
        virtual const std::string& readLine() = delete;

        struct {
            std::string current;
            unsigned int number;
            unsigned short offset;
        } line;


        //Parser knows how to personalize TokenizeLine function with arguments.

        static void InitTokenizers();
        static void InitFlags();

        // flags/particulars indicate which ascii codes are allowed to be in tokens?
        //static Token TokenizeLine(std::string str, std::vector<std::pair<ASCII, bool>> flags, std::vector<std::pair<char, bool>> particulars, int numTokens = str.length);


        //static std::vector<bool> flags;
        static std::vector<std::function<bool(char, unsigned short)>> tokenizers;
    };
};


#endif
