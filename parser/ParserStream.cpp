#include "ParserStream.h"

namespace Caspar {
    std::vector<std::function<bool(char, unsigned short)>> ParserStream::tokenizers;

    void ParserStream::Init() {
        InitTokenizers();
    }

    // Only giving offset as extra information implies that only patterns in beginning, finite length of token will be important for categorization (ie lower-upper-lower-upper-... pattern could not be used as a category).
    void ParserStream::InitTokenizers() {
        //std::cout << "resizing tokenizers to " << int(_ffs(Token::Last)) << "\n";
        tokenizers.resize(_ffs(Token::Last));

        tokenizers[_ffs(Token::Invalid)] = [](char c, unsigned short off) -> bool {
            _unused(off, c);
            return false;
        };
        tokenizers[_ffs(Token::Delimiter)] = [](char c, unsigned short off) -> bool {
            _unused(off);
            if(c == ' ' || c == '\n') return true;
            else return false;
        };
        tokenizers[_ffs(Token::Indent)] = [](char c, unsigned short off) -> bool {
            _unused(off);
            if(c == '\t') return true;
            else return false;
        };

        tokenizers[_ffs(Token::EOE)] = [](char c, unsigned short off) -> bool {
            if(off == 0 && c == 0x0) return true;
            else return false;
        };
        tokenizers[_ffs(Token::SOE)] = [](char c, unsigned short off) -> bool {
            if(off == 0 && c == '\r') return true;
            else return false;
        };

        tokenizers[_ffs(Token::Symbol)] = [](char c, unsigned short off) -> bool {
            const int ci = c;
            if(off > 0 && ((ci >=  48 && ci <= 57) || ci == 95)) return true;
            else if((ci >= 65 && ci <= 90) ||  (ci >= 97 && ci <= 122)) return true;
            else if(off == 0 && c == '@') return true;
            else return false;
        };

        tokenizers[_ffs(Token::Number)] = [](char c, unsigned short off) -> bool {
            _unused(off);
            const int ci = c;
            if(((ci >=  48 && ci <= 57) || ci == 46)) return true;
            else return false;
        };

        tokenizers[_ffs(Token::Special)] = [](char c, unsigned short off) -> bool {
            _unused(off); const int ci = c;
            if(c == 36 || c == 96) return false;//Skip intrins $ and `
            else if((33 <= ci && ci <= 47) || (58 <= ci && ci <= 64) || (91 <= ci && ci <= 96) || (123 <= ci && ci <= 126)) return true;
            return false;
        };
        tokenizers[_ffs(Token::Intrin)] = [](char c, unsigned short off) -> bool {
            static thread_local char prev = 0x0;
            if(off != 0 && (c != '$' || (prev != '$' && c != prev)) ) return false;//Force single-character intrins
            prev = c;

            if(c == 36 || c == 96) return true;
            else return false;
        };
        tokenizers[_ffs(Token::String)] = [](char c, unsigned short off) -> bool {
            _unused(off); _unused(c);
            return false;
        };
        tokenizers[_ffs(Token::Raw)] = [](char c, unsigned short off) -> bool {
            _unused(off); _unused(c);
            return false;
        };
    }
    /*
    void ParserStream::InitFlags() {
        flags.resize(ASCII::EOE);

        for(int i = 0; i < flags.length; i++) {
            flags[i] = std::pair<ASCII, bool>(static_cast<ASCII>(i), true);
        }
    }*/


    TokenList ParserStream::TokenizeLine(std::string line, Token::Type types, const std::string& extraTypes, unsigned int lineID) {
        _unused(extraTypes);

        std::list<Token*> ret;
        unsigned int lineOffset = 0;

        ret.push_back(new Token{Token::SOE, "\r", -1, lineID});

        while(lineOffset < line.length()) {
            unsigned int typeShift = types;
            unsigned char typeOffset = 0;

            struct {
                unsigned int length = 0, type = Token::Invalid;
            } best;

            while(typeShift > 0) {//Upper bound of n flags (ie, bits before the EOE constant)
                if(bool(typeShift & 0x1)) {
                    unsigned int functionIdx = 0x1 << typeOffset;
                    auto function = tokenizers[_ffs(functionIdx)];

                    unsigned int tokenLength = 0;
                    for(unsigned int i = 0; i < line.length(); i++) {
                        bool matched = function(line[lineOffset + i], i);
                        //std::cout << "\t\t\tf(" << line[lineOffset+i] << ") = " << int(matched) << "\n";

                        if(matched) tokenLength++;
                        else break;
                    }

                    //std::cout << "\t\ttrying type " << functionIdx << " at " << lineOffset << " for length " << tokenLength << "\n";

                    if(best.length < tokenLength) {
                        best.length  = tokenLength;
                        best.type = functionIdx;
                    }

                }

                typeOffset++;
                typeShift = typeShift >> 1;
            }

            if(best.length == 0) best.length = 1;//Skip invalid characters
            //std::cout << "\tadding token from " << lineOffset << " to " << (lineOffset + best.length) << "\n";


            ret.push_back(new Token{static_cast<Token::Type>(best.type), line.substr(lineOffset, best.length), lineOffset, lineID});
            lineOffset += best.length;
        }

        if(ret.size() == 1) ret.clear();
        else ret.push_back(new Token{Token::EOE, "\n", lineOffset, lineID});
        return ret;
    }

    ParserStream::ParserStream() : child(nullptr) {
    

    }

    ParserStream::~ParserStream() {

    }

    bool ParserStream::open(const std::string& path) {
        _unused(path);
       return true;
    }
    void ParserStream::close() {

    }

    std::list<Token*> ParserStream::getStatement(Token::Type types, const std::string extraDelimiters) {
        if(child != nullptr) {
            if(child->completed()) {
                delete child;
                child = nullptr;
            } else return child->getStatement(types,extraDelimiters);
        }
        return this->readTokens(types, extraDelimiters);
    }

    void ParserStream::push(ParserStream *stream) {
        if(child != nullptr) return child->push(stream);
        else child = stream;
    }
    void ParserStream::pop() {
        if(child != nullptr) return child->pop();
        else {
            if(parent == nullptr) {
                std::cerr << "ParserStream::pop called on root stream, ignoring...\n";
                return;
            }
            parent->child = nullptr;
            delete this;
        }
    }

    bool ParserStream::completed() const {
        return true;
    }
    std::list<Token*> ParserStream::readTokens(Token::Type types, const std::string extraDelimiters) {
        _unused(types, extraDelimiters);
        return std::list<Token*>();
    }
    /*
    Token ParserStream::readToken() {
        Token ret;

        bool types[Token::EOE] = {true};
        //if(tokenizers[_ffs(Tokens::Delimiter)](firstChar, line.offset)) {//It's a delimiter

        unsigned short numTrue;
        for(unsigned short i = line.offset; i < line.current.size(); i++) {
            for(unsigned short j = 0; j < Token::EOE; j++) {
                if(types[j]) types[j] = tokenizers[_ffs(j)](line.current[line.offset], i - line.offset);
            }

            numTrue = 0;
            for(unsigned short j = 0; j < Token::EOE; j++) {
                if(types[j]) numTrue++;
            }
            if(numTrue == 1) break;
        }
        if(numTrue > 1) throw std::runtime_error("Yo dumb ass passed an ambiguous token");
        if(numTrue == 0) throw std::runtime_error("set of token defs. not onto set of possible strings");

        int trueIndex;
        for(unsigned short i =  0; i < Token::EOE; i++) {
            if(types[i]) {
                trueIndex = i;
                break;
            }
        }

        //idk syntax
        ret.type = Token::Type::static_cast<Token::Type>(trueIndex);
        ret.lineNumber = line.number;
        ret.offset = line.offset;

        return ret;
    }*/
    /*
    unsigned int ParserStream::lineNumber() const {
        return line.number;
    }

    unsigned short ParserStream::tokenOffset() const {
        return line.offset;
    }*/
};
