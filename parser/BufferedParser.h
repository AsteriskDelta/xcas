#ifndef INC_BUFFEREDPARSER_H
#define INC_BUFFEREDPARSER_H
#include "iCaspar.h"
#include "ParserStream.h"
#include "CyclicVector.h"

namespace Caspar {
    class BufferedParser : public ParserStream {
    public:
        static constexpr unsigned int DefaultSize = 1024 * 8;//8kb buffer by default

        BufferedParser();
        virtual ~BufferedParser();

        virtual bool open(const std::string& path) override;
        //virtual void close() override;

        //Emplaces all data and returns true, or emplaces NONE of it and returns false if the buffer is full
        virtual bool write(const std::string& data);

        //Returns a non-empty list iff an entire statement (generally a line) can be read

        virtual std::list<Token*> readTokens(Token::Type types = Token::All, const std::string extraDelimiters = "");

        virtual bool completed() const override;
        bool isEmpty() const;
        
        //Return  empty line when reading file, otherwise ALWAYS skip empty  lines and return the next valid (non-empty) one
        //virtual const std::string& readLine() override;

    protected:
        CyclicVector<char> buffer;
        std::string bufferName;
        unsigned int lineCnt;
    };
};


#endif
