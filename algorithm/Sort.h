#ifndef _unused
#define _unused(x) ((void(x)))
#endif
typedef unsigned int Uint;

namespace Sort {
  template <class T> bool isSorted	(T *const data, const Uint count);
  
  template <class T> T* InsertionSort	(T *const data, const Uint count, Uint gap = 0);
  template <class T> T* QuickSort	(T *const data, const Uint count, Uint ref = 0);
  
  void _unitTest();
};
