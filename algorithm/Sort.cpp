#include "Sort.h"
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <iostream>
#include "Timer.h"

template <typename T> inline T median(const T a, const T b, const T c) {
  using namespace std;
  return max(min(a,b), min(max(a,b),c));
}

namespace Sort {
  template <class T> bool isSorted(T *const data, const Uint count) {
    if(count == 1) return true;
    T *edge = &data[count-1];
    
    while(edge > data) {
      if(*(edge-1) > *edge) return false;
      edge--;
    }
    
    return true;
  }
  
  //static float gapExp = 0.93462f;
  #define INSSRT_GAP(x) std::max( pow((x)/3, 0.8862f) - 1, 1.0) //Obtained this by gradient descent optimization of run-time space
  template <class T> T* InsertionSort(T *const data, const Uint count, Uint gap) {
    if(gap == 0) gap = INSSRT_GAP(count);//Gap heuristic for interleaved list
    
    while(gap > 0) {//std::cout << gap << "->";
      for(Uint i = gap; i < count; i++) {
	T *current = &data[i], *previous = &data[i-gap];
	
	while(previous >= data && *current < *previous) {
	  std::swap(*current, *previous);
	  current = previous; previous -= gap;
	}
	
      }
      
      if(gap == 1) break;
      else gap = INSSRT_GAP(gap);
    }
    
    return data;
  }
  
  template <class T> T* QuickSort(T *const data, const Uint count, Uint ref) {
    _unused(ref);
    if(count <= 40) return InsertionSort(data, count, 1);//data;
    
    T pivot = median(data[count / 4], data[count / 2], data[count * 3 / 4]);
    //T pivot = data[count / 2];
    T *left = &data[0], *right = &data[count - 1];
    while (left <= right) {
        if (*left < pivot) left++;
        else if (*right > pivot) right--;
        else {
	  std::swap(*left, *right);
          left++; right--;
        }
    }
    
    QuickSort(data, right - data + 1, 0);
    QuickSort(left, &data[count] - left, 0);
    
    return data;
  }
  
  //Assuming I've not offended any deities lately, and that testArr is not, in fact, one million some randomly generated, ordered items
  template <class T> inline void randomizeArray(T *arr, Uint count) {
    for(Uint i = 0; i < count; i++) {
      arr[i] = rand()%2;
    }
  };
  template <class T> inline void printArray(T *arr, Uint count) {
    for(Uint i = 0; i < count; i++) {
      std::cout << arr[i];
      if(i != count-1) std::cout << ",";
    }
    std::cout << "\n";
  };
  
  static auto sortPtr = &InsertionSort<Uint>;
  void _unitTestSort(std::string name, Uint ref) {
    const Uint testCount = 1024*1024*4, testIterations = 10;
    
    Uint *testArr = new Uint[testCount];
    randomizeArray(testArr, testCount);
    Timer timer;
    
    double averageMS = 0.0;
    std::cout << name << " test...\n";
    
    for(Uint test = 0; test < testIterations; test++) {
      //std::cout << "random: "; printArray(testArr, testCount);
      timer.Start();
      testArr = (*sortPtr)(testArr, testCount, ref);
      timer.Stop();
      //std::cout << "sorted: "; printArray(testArr, testCount);
      
      assert(isSorted(testArr, testCount));
      randomizeArray(testArr, testCount);
      double reqTime = timer.getElapsedMilli();
      std::cout << "Test " << test << " completed with " << testCount << " items in \t" << reqTime << "ms\n";
      if(test!=0) averageMS += reqTime;
    }
    averageMS /= double(testIterations-1);
    std::cout << "Average " << name << " time: \t" << (averageMS) << "ms\n\n";
  }
  /*Exponent optimization stuff
   * float best = 0.8f; double bestAvg = 10000.0;
    for(gapExp = 0.8f; gapExp < 0.99f; gapExp += 0.005f) {
      std::cout << "EXEC GAP EXP: " << gapExp << ":\n";
   * 
    if(averageMS < bestAvg) {
      bestAvg = averageMS;
      best = gapExp;
    }
    std::cout << "Optimal gap exponent: " << best << " at " << bestAvg << "ms\n";
  }
   */
  
  void _unitTest() {
    srand(time(NULL));
    
    sortPtr = &InsertionSort<Uint>;
    //_unitTestSort("Trivial InserationSort", 1);
    _unitTestSort("Optimized InserationSort", 0);
    
    sortPtr = &QuickSort<Uint>;
    _unitTestSort("QuickSort", 0);
  }
};
