#ifndef CASPAR_OPSCOPE_H
#define CASPAR_OPSCOPE_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpIncScope : public Symbol {
    public:
        OpIncScope();
        ~OpIncScope();

        virtual bool executeOn(XNode *node, VM *const vm) override;
    };
    class OpDecScope : public Symbol {
    public:
        OpDecScope();
        ~OpDecScope();

        virtual bool executeOn(XNode *node, VM *const vm) override;
    };
};

#endif
