#ifndef CASPAR_OP_PAREN_H
#define CASPAR_OP_PAREN_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpParen : public Symbol {
    public:
        OpParen();
        ~OpParen();

        virtual bool executeOn(XNode *node, VM *const vm) override;
        static void Bootstrap(VM *const vm);
    };

};

#endif

