#ifndef CASPAR_OP_MISC_PRINTR_H
#define CASPAR_OP_MISC_PRINTR_H
#include "iCaspar.h"
#include "Symbol.h"
#include <sstream>

namespace Caspar {
    class Misc_Printr : public Symbol {
    public:
        inline Misc_Printr() {
            this->type = XNode::Identifier;
        }
        inline ~Misc_Printr() {

        }

        inline virtual bool executeOn(XNode *node, VM *const vm) override {
            //std::cout << "COMMENT: " << node->str() << "\n";
            _unused(vm);
            bool first = true;
            std::stringstream ss;
            for(auto it = ++node->begin(); bool(it); ++it) {
                it->setFlag(XNode::Stale);
                if(it->type == XNode::Delimiter) continue;
                
                if(!first) ss << " ";
                else first = false;

                ss << it->str();
            }
            std::cout << ss.str() << "\n";
            return false;
        }
    };
};

#endif
