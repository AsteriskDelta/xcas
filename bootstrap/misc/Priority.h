#ifndef CASPAR_OP_MISC_PRIORITY_H
#define CASPAR_OP_MISC_PRIORITY_H
#include "iCaspar.h"
#include "Symbol.h"
#include <sstream>

namespace Caspar {
    class Misc_Priority : public Symbol {
    public:
        inline Misc_Priority() {
            this->type = XNode::Identifier;
        }
        inline ~Misc_Priority() {

        }
        /*
        inline virtual bool executeOn(XNode *node, VM *const vm) override {
            if(node->firstChild == nullptr){
                std::cerr <<"Priority property called on an object without a pattern!\n";
                return false;
            }

            class Symbol *sym = dynamic_cast<class Symbol*>(node->firstChild);
            if(sym == nullptr){
                std::cerr <<"Priority property called on non-symbol!\n";
                return false;
            }

            //pattern->priority = atoi(node->firstChild->next->str().c_str());
            sym->executionPriority(atoi(node->firstChild->next->str().c_str()));
            delete node->firstChild->next;
            node->firstChild->next = nullptr;

            node->replaceWith(node->firstChild);
            std::cout <<"set node execution priority to " << sym->executionPriority() <<" for " << node->str() << "\n";
            return true;
        }*/
        
        virtual void propertyAssigned(Symbol *owner, XNode *node) override {
            if(node->next == nullptr) {
                std::cerr << "Priority called without a number following it!\n";
            }
            std::cout << "set execution priority of " << owner->str() << " to " << node->next->str() << "\n";
            owner->executionPriority(atoi(node->next->str().c_str()));
            node->spoil();
            node->next->spoil();
        }
    };
};

#endif

