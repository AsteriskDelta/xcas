#ifndef CASPAR_OP_MISC_COMMENT_H
#define CASPAR_OP_MISC_COMMENT_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class Misc_Comment : public Symbol {
    public:
        inline Misc_Comment() {
            this->type = XNode::Identifier;
        }
        inline virtual ~Misc_Comment() {

        }

        inline virtual bool executeOn(XNode *node, VM *const vm) override {
            std::cout << "COMMENT: " << node->str() << "\n";
            _unused(vm);
            //Don't evaluate contents of a pattern
            //for(auto child = node->firstChild; child != nullptr; child = child->next) child->setFlag(XNode::Stale);
            node->spoil();
            return false;
        }
    };
};

#endif
