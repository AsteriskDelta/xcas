#ifndef CASPAR_OP_MISC_PATTPRIOR_H
#define CASPAR_OP_MISC_PATTPRIOR_H
#include "iCaspar.h"
#include "Symbol.h"
#include <sstream>

namespace Caspar {
    class Misc_PatternPriority : public Symbol {
    public:
        inline Misc_PatternPriority() {
            this->type = XNode::Identifier;
        }
        inline ~Misc_PatternPriority() {

        }

        inline virtual bool executeOn(XNode *node, VM *const vm) override {
            std::cout << "PATTERN PRIORITY: " << node->str() << "\n";
            _unused(vm);
            node->debugTree();
            if(node->firstChild == nullptr){
                std::cerr <<"PatternPriority property called on an object without a pattern!\n";
                return false;
            }

            class Pattern *pattern = dynamic_cast<class Pattern*>(node->firstChild);
            if(pattern == nullptr){
                std::cerr <<"PatternPriority property called on non-pattern!\n";
                return false;
            }

            pattern->priority = atoi(node->firstChild->next->str().c_str());
            delete node->firstChild->next;
            node->firstChild->next = nullptr;

            node->replaceWith(node->firstChild);
            std::cout <<"set node priority to " << pattern->priority <<" for " << node->str() << "\n";
            return true;
        }
    };
};

#endif
