#ifndef CASPAR_OPDEFINE_H
#define CASPAR_OPDEFINE_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpDefine : public Symbol {
    public:
        OpDefine();
        ~OpDefine();
        
        virtual bool executeOn(XNode *node, VM *const vm) override;
        
        static void Bootstrap(VM *const vm);
    };
};

#endif
