#include "OpPattern.h"
#include "Symbol.h"
#include "String.h"
#include "Pattern.h"
#include "VM.h"
#include "Pattern.h"
#include "String.h"

namespace Caspar {
    OpPattern::OpPattern() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpPattern::~OpPattern() {

    }

    bool OpPattern::executeOn(XNode *node, VM *const vm) {
        node->resolve(vm);
        //Symbol *sym = node->symbol();

        class Pattern *patt = new class Pattern();
        XNode *from = nullptr;
        XNode *to = nullptr;//Symbol::Reference("",vm);//new XNode();

        unsigned int placeholderID = 0;
        for(XNode *child = node->firstChild; child != nullptr; child = child->next) {
          XNode *app = nullptr;

          if(!child->hasFlag(XNode::Resolved)) {//Add it as a literal
              app = new Types::String(child->str());
                std::cout << "creating pattern lit \"" << child->str() << "\" -> " << app->str() << ", xnode " << static_cast<XNode*>(app)->str() << "\n";
          } else {
              child->subID = placeholderID;
              app = child->clone(true, false);
              app->token = nullptr;//Make sure we don't attempt to reference the child's (deleted) token later
              if(child->hasFlag(XNode::Placeholder)) {
                  XNode *toApp = child->clone(true, false);
                  toApp->token = nullptr;
                  //to->addChild(toApp);//Pass arguements as placeholders
                  if(to == nullptr) to = toApp;
                  else to->append(toApp);
              }
              std::cout << "cloned child " << child->infoString() << " for lit " << app->infoString() << "\n";
              placeholderID++;
          }

            if(app != nullptr) {
                if(from == nullptr) from = app;
                else from->append(app);
            }
        }

        //Remove the stale flag to match (possibly) unexecuted code
        from->freshen();
        if(to != nullptr) to->freshen();

        std::cout << "OpPattern execute, from:\n";
        from->debugTree();
        std::cout << "to:\n";
        //to = to->resolve(vm);
        if(to != nullptr) to->debugTree();

        patt->symbolPtr = vm->scope->Scope::resolve(patt->typeName());
        patt->setFrom(from);
        patt->setTo(to);

        std::cout << "PATTERN PTRS @ " << patt <<":\n\t" << patt->from << " @ " << &(patt->from) << "\n\t" << patt->to << " @ " << &(patt->from)<<"\n";
        node->replaceWith(patt);
        patt->spoil();//Mark as stale so we don't attempt to execute the newly created pattern
        delete node;

        //std::cout << "!!!OpPattern got " << node->str() << " = " << from->str() << "\n";
        return true;
    }

    void OpPattern::Bootstrap(VM *const vm) {
        OpPattern *op = new OpPattern();
        op->name("opPattern", vm->scope);

        class Pattern *defPattern = new class Pattern();

        {//The precedent
            XNode *beg = new Types::String("`"), *end = new Types::String("`");
            XNode *patternSrc = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            beg->append(patternSrc);
            patternSrc->append(end);

            defPattern->from = beg;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opPattern", vm);
            XNode *patternStr = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            opRoot->addChild(patternStr);
            defPattern->to = defPattern->write = opRoot;
        };
        op->executionPriority(defPattern->priority = 1500);
        defPattern->registerTo(vm);
    }
};
