#ifndef CASPAR_BOOTSTRAP_H
#define CASPAR_BOOTSTRAP_H
#include "iCaspar.h"

namespace Caspar {
    class VM;
    void BootstrapVM(VM *const vm);
};

#endif
