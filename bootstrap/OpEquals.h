#ifndef CASPAR_OPEQUALS_H
#define CASPAR_OPEQUALS_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpEquals : public Symbol {
    public:
        OpEquals();
        ~OpEquals();

        virtual bool executeOn(XNode *node, VM *const vm) override;
    };
};

#endif
