#include "OpInclude.h"
#include "Symbol.h"
#include "String.h"
#include "Pattern.h"
#include "VM.h"
#include "Pattern.h"
#include "String.h"
#include "ParserFileStream.h"

namespace Caspar {
    OpInclude::OpInclude() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpInclude::~OpInclude() {

    }

    bool OpInclude::executeOn(XNode *node, VM *const vm) {
        std::string path = node->str();
        std::cout << "OpInclude(" << path << ")\n";

        ParserFileStream *newStream = new ParserFileStream();
        if(!newStream->open(path)) {
            std::cerr << "Unable to open file for inclusion at \"" << path << "\"\n";
            return false;
        }

        vm->stream()->push(newStream);

        return true;
    }

    void OpInclude::Bootstrap(VM *const vm) {
        OpInclude *op = new OpInclude();
        op->name("opInclude", vm->scope);

        class Pattern *defPattern = new class Pattern();

        {//The precedent
            XNode *beg = new Types::String("@include"), *beg2 = new Types::String("\""), *end = new Types::String("\"");
            XNode *patternSrc = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            beg->append(beg2);
            beg->append(patternSrc);
            patternSrc->append(end);

            defPattern->from = beg;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opInclude", vm);
            XNode *patternStr = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            opRoot->addChild(patternStr);
            defPattern->to = defPattern->write = opRoot;
        };

        defPattern->registerTo(vm);
    }



    OpIncludeStd::OpIncludeStd() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpIncludeStd::~OpIncludeStd() {

    }

    bool OpIncludeStd::executeOn(XNode *node, VM *const vm) {
        std::string path = node->firstChild->str();
        std::cout << "OpIncludeStd(" << path << ")\n";

        ParserFileStream *newStream = new ParserFileStream();
        if(!newStream->open(path)) {
            std::cerr << "Unable to open file for inclusion at \"" << path << "\"\n";
            return false;
        }

        vm->stream()->push(newStream);

        return true;
    }

    void OpIncludeStd::Bootstrap(VM *const vm) {
        OpIncludeStd *op = new OpIncludeStd();
        op->name("opIncludeStd", vm->scope);

        class Pattern *defPattern = new class Pattern();

        {//The precedent
            XNode *beg = new Types::String("@include"), *beg2 = new Types::String("<"), *end = new Types::String(">");
            XNode *patternSrc = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            beg->append(beg2);
            beg->append(patternSrc);
            patternSrc->append(end);

            defPattern->from = beg;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opIncludeStd", vm);
            XNode *patternStr = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            opRoot->addChild(patternStr);
            defPattern->to = defPattern->write = opRoot;
        };

        defPattern->registerTo(vm);
    }
};
