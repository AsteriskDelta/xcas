#include "OpParen.h"
#include "Symbol.h"
#include "String.h"
#include "Pattern.h"
#include "VM.h"
#include "Pattern.h"
#include "String.h"

namespace Caspar {
    OpParen::OpParen() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpParen::~OpParen() {

    }

    bool OpParen::executeOn(XNode *node, VM *const vm) {
        node->resolve(vm);


        //node->replaceWith(node->firstChild);

        return true;
    }
    
    void OpParen::Bootstrap(VM *const vm) {
        OpParen *op = new OpParen();
        op->name("paren", vm->scope);

        class Pattern *defPattern = new class Pattern();

        {//The precedent
            XNode *beg = new Types::String("("), *end = new Types::String(")");
            XNode *patternSrc = XNode::MakePlaceholder(0, XNode::Expression, XNode::Wildcard);

            beg->append(patternSrc);
            patternSrc->append(end);

            defPattern->from = defPattern->write = beg;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("paren", vm);
            XNode *patternStr = XNode::MakePlaceholder(0, XNode::Expression, XNode::Wildcard);

            opRoot->addChild(patternStr);
            defPattern->to = opRoot;
        };
        defPattern->priority = 1100;
        op->executionPriority(-1100);
        defPattern->registerTo(vm);
    }

};
