#ifndef CASPAR_OPTO_H
#define CASPAR_OPTO_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpTo : public Symbol {
    public:
        OpTo();
        ~OpTo();

        virtual bool executeOn(XNode *node, VM *const vm) override;
        static void Bootstrap(VM *const vm);
    };
};

#endif
