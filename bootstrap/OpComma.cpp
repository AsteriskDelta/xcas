#include "OpComma.h"
#include "Symbol.h"
#include "String.h"
#include "VM.h"
#include "Pattern.h"
#include "type/List.h"

namespace Caspar {
    OpComma::OpComma() : Symbol() {
        this->type = XNode::Identifier;
    }
    OpComma::~OpComma() {

    }

    bool OpComma::executeOn(XNode *node, VM *const vm) {
        _unused(vm);
        //node->resolve(vm);
        XNode *a = node->firstChild->firstChild, *b = node->firstChild->next->firstChild;
        /*Types::List *list;

        if(a->typeName() == "list") {
            list = dynamic_cast<Types::List*>(a);
            list->push(b->clone(true,true));
        } else {
            list = new Types::List();
            list->push(a->clone(true,true));
            list->push(b->clone(true,true));
        }

        node->replaceWith(list);
        delete node;
        */
        XNode *listNode = (node->parent != nullptr)? node->parent->parent : nullptr;
        Types::List *list;
        
        if(listNode != nullptr && listNode->typeName() == "list") {
            list = dynamic_cast<Types::List*>(listNode);
            //list->push(a->clone(true,true));
            node->replaceWith(a->clone(true,true));
            list->push(b->clone(true,true));
            //node->relinquishTo(nullptr);
            delete node;
        } else {
            list = new Types::List();
            list->push(a->clone(true,true));
            list->push(b->clone(true,true));
            node->replaceWith(list);
            delete node;
        }
        
        return true;
    }

    void OpComma::Bootstrap(VM *const vm) {
        OpComma *op = new OpComma();
        class Pattern *dPattern = new class Pattern();
        op->name("opComma", vm);

        {//The precedent
            XNode *iden = XNode::MakePlaceholder(0, XNode::Expression, XNode::Wildcard);//new XNode();
            //iden->setFlag(XNode::PlaceHolder | XNode::Wildcard);
            //iden->setType(XNode::Identifier);

            XNode *chr = new Types::String(",");

            XNode *props = XNode::MakePlaceholder(1, XNode::Expression, XNode::Wildcard);//new XNode();
            //props->setFlag(XNode::PlaceHolder | XNode::Wildcard);
            //props->setType(XNode::Expression);


            iden->append(chr);
            chr->append(props);

            dPattern->from = iden;
            dPattern->write =iden;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opComma", vm);
            XNode *propContainer = XNode::MakeContainer(), *exprContainer = XNode::MakeContainer(),
                  *propArg = XNode::MakePlaceholder(0, XNode::Expression, XNode::Wildcard),
                  *exprArg = XNode::MakePlaceholder(1, XNode::Expression, XNode::Wildcard);

            propContainer->addChild(propArg);
            exprContainer->addChild(exprArg);

            opRoot->addChild(propContainer);
            opRoot->addChild(exprContainer);
            dPattern->to = opRoot;
        };

        /*{//And finally, output
            XNode *iden = XNode::MakePlaceholder(0, XNode::Identifier, XNode::Wildcard);//new XNode();
            XNode *chr = new String(":");
            XNode *props = XNode::MakePlaceholder(1, XNode::Expression, XNode::Wildcard);//new XNode();

            iden->append(chr);
            chr->append(props);

            dPattern->setFrom(iden);
        };*/
        
        dPattern->priority = -1200;

        dPattern->registerTo(vm);
    }
};
