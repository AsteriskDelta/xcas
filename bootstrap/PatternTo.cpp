#include "PatternTo.h"
#include "VM.h"
#include "Pattern.h"

namespace Caspar {
    Misc_PatternTo::Misc_PatternTo() {
        this->type = XNode::Identifier;
    }
    Misc_PatternTo::~Misc_PatternTo() {

    }

    bool Misc_PatternTo::executeOn(XNode *node, VM *const vm) {
        class Pattern *patt = new class Pattern();
        XNode *from = nullptr;
        XNode *to = nullptr;
        //class Pattern* subPatt = dynamic_cast<class Pattern*>(it);

        //unsigned int i = 0;
        //std::cout << "PATTERNTO\n";
        from = dynamic_cast<class Pattern*>(node->firstChild)->from->clone(true,true);
        to = dynamic_cast<class Pattern*>(node->firstChild->next)->from->clone(true,true);//new Symbol();//Return values are nested within a function specifier
        //to->addChild(dynamic_cast<class Pattern*>(node->firstChild->next)->to->clone(true,true));
        //std::cout << "\tfrom = " << from << "\n\tto = " << to << "\n";
        /*for(auto it = node->firstChild; it != nullptr; it = it->next) {

            patt->orders[i] = subPatt->from->clone(true,true);//it->symbol()->getPattern();//dynamic_cast<class Pattern*>(it)->orders[0];
            std::cout << "\torder " << i << " loc@ " << subPatt << ":\t" << patt->orders[i] << " @ " << &(dynamic_cast<class Pattern*>(it)->from) << " from " << it << "\n";
            i++;
            for(int j = 0; j < 3; j++) {
                std::cout << "\t\t" << j << ": " << subPatt->orders[j] << "\n";
            }
            if(i >= 3) break;
        }*/

        patt->symbolPtr = vm->scope->Scope::resolve(patt->typeName());
        node->replaceWith(patt);
        patt->setFrom(from);
        patt->setTo(to);
        patt->setFlag(XNode::Stale);//Mark as stale so we don't attempt to execute the newly created pattern
        delete node;

        return true;
    }
}
