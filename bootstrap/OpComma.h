#ifndef CASPAR_OPCOMMA_H
#define CASPAR_OPCOMMA_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpComma : public Symbol {
    public:
        OpComma();
        ~OpComma();

        virtual bool executeOn(XNode *node, VM *const vm) override;

        static void Bootstrap(VM *const vm);
    };
};

#endif
