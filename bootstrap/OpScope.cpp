#include "OpScope.h"
#include "Symbol.h"
#include "String.h"
#include "Pattern.h"
#include "VM.h"
#include "Pattern.h"
#include "String.h"
#include "Block.h"

namespace Caspar {
    OpIncScope::OpIncScope() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpIncScope::~OpIncScope() {

    }

    bool OpIncScope::executeOn(XNode *node, VM *const vm) {
        node->resolve(vm);

        class Block *block = new class Block();
        vm->pushScope(block);

        auto ref = Symbol::Reference(block, vm);

        node->replaceWith(ref);

        return true;
    }

    OpDecScope::OpDecScope() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpDecScope::~OpDecScope() {

    }

    bool OpDecScope::executeOn(XNode *node, VM *const vm) {
        node->resolve(vm);

        vm->popScope();

        //node->replaceWith(node->firstChild);

        return true;
    }
};
