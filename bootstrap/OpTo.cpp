#include "OpTo.h"
#include "Symbol.h"
#include "String.h"
#include "Pattern.h"
#include "VM.h"
#include "Pattern.h"
#include "String.h"

namespace Caspar {
    OpTo::OpTo() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpTo::~OpTo() {

    }

    bool OpTo::executeOn(XNode *node, VM *const vm) {
        node->resolve(vm);

        //std::cout << "!!!OpTo got " << node->str() << "\n";
        //node->debugTree();
        if(node->firstChild == nullptr){
            //std::cerr <<"OpTo property called on an object without a pattern!\n";
            return false;
        }

        class Pattern *pattern = dynamic_cast<class Pattern*>(node->firstChild);
        class Pattern *opattern = dynamic_cast<class Pattern*>(node->firstChild->next);
        if(pattern == nullptr){
            //std::cerr <<"OpTo property called on non-pattern!\n";
            return false;
        }

        pattern->to = opattern->from->clone(true,true);
        pattern->next = nullptr;
        delete opattern;
        node->replaceWith(node->firstChild);

        return true;
    }

    void OpTo::Bootstrap(VM *const vm) {
        OpTo *op = new OpTo();
        op->name("opTo", vm->scope);
    /*
        class Pattern *defPattern = new class Pattern();

        {//The precedent
            XNode *beg = new Types::String("`"), *end = new Types::String("`");
            XNode *patternSrc = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            beg->append(patternSrc);
            patternSrc->append(end);

            defPattern->from = beg;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opTo", vm);
            XNode *patternStr = XNode::MakePlaceholder(0, XNode::Literal, XNode::Wildcard);

            opRoot->addChild(patternStr);
            defPattern->to = defPattern->write = opRoot;
        };

        //defPattern->priority =

        defPattern->registerTo(vm);*/
    }
};
