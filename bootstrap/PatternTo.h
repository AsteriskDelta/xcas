#ifndef CASPAR_OP_MISC_PATTERNTO_H
#define CASPAR_OP_MISC_PATTERNTO_H
#include "iCaspar.h"
#include "Symbol.h"
#include "Pattern.h"

namespace Caspar {
    class Misc_PatternTo : public Symbol {
    public:
        Misc_PatternTo();
        virtual ~Misc_PatternTo();

        virtual bool executeOn(XNode *node, VM *const vm) override;
    };
};

#endif
