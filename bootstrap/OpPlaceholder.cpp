#include "OpPlaceholder.h"
#include "Symbol.h"
#include "String.h"
#include "VM.h"
#include "Pattern.h"
#include <stdlib.h>
namespace Caspar {
    OpPlaceholder::OpPlaceholder() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpPlaceholder::~OpPlaceholder() {

    }

    OpPlaceholderWildcard::OpPlaceholderWildcard() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpPlaceholderWildcard::~OpPlaceholderWildcard() {

    }

    bool PlaceholderExec(XNode *node, VM *const vm, unsigned int flags) {
        node->resolve(vm);
        if(node->firstChild != nullptr) node->children()[0].resolve(vm);
        else return false;//We need a child to know what we're replac(ing/ed with)
        uint8_t pcSubID = 0xFF;

        auto childSymbol = node->children()[0].symbol();

        //if(false && childSymbol == nullptr /*|| !childSymbol->resolve(vm)*/) {
        //  std::cerr << "Unable to resolve placeholder for node\n";
        //  return false;

        if(childSymbol == nullptr) {
          //std::cerr << "Unable to resolve placeholder for node with null child\n";
          //It might be a placeholder ID Literal

            pcSubID = strtol(node->firstChild->raw().c_str(), nullptr, 0);
            if(pcSubID == 0 && node->firstChild->raw()[0] != '0') {
                std::cerr << "Unable to resolve placeholder symbol \"" << node->firstChild->raw() << "\"\n";
                return false;
            }

        }

        std::string name = node->str();//childSymbol->str();
        //Symbol *sym = node->symbol();
        //if(sym->firstChild == nullptr) return false;

        std::string placeholderType = name;


        std::cout << "!!!OpPlaceholder got " << name << " ("<<node->infoString()<<", "<< placeholderType<<"\n";//"<<childSymbol->infoString()<<") = " << placeholderType << "\n";
        //sleep(3);
        Symbol *typeSymbol = vm->scope->Scope::resolve(placeholderType);
        //if(typeSymbol == nullptr) {

        if(childSymbol != nullptr) std::cout << "!!!OpPlaceholder got " << name << " ("<<node->infoString()<<", "<<childSymbol->infoString()<<") = " << name << "\n";
        else std::cout << "!!!OpPlaceholder got #" << int(pcSubID) << "\n";
        //sleep(3);
        //Symbol *typeSymbol = vm->resolve(placeholderType);
        /*if(typeSymbol == nullptr) {
          std::cerr << "Unable to resolved placeholder type '" << placeholderType << "'\n";
          return false;
      }*/

        node->symbolPtr = childSymbol;
        node->type = XNode::Identifier;
        node->setFlag(XNode::Placeholder | flags);
        node->subID = pcSubID;
        node->token = nullptr;
        //delete node->firstChild;
        node->firstChild = nullptr;

        return true;
    }

    bool OpPlaceholder::executeOn(XNode *node, VM *const vm) {
        return PlaceholderExec(node,vm,0x0);
    }

    bool OpPlaceholderWildcard::executeOn(XNode *node, VM *const vm) {
        return PlaceholderExec(node,vm,XNode::Wildcard);
    }

    void OpPlaceholder::Bootstrap(VM *const vm) {
        OpPlaceholder *op = new OpPlaceholder();
        OpPlaceholderWildcard *opw = new OpPlaceholderWildcard();
        class Pattern *dPattern = new class Pattern(), *dPattern2 = new class Pattern();
        op->name("opPlaceholder", vm);
        opw->name("opPlaceholderWildcard", vm);

        {//The precedent
            XNode *iden = XNode::MakePlaceholder(0, XNode::Identifier, 0x0);
            XNode *iden2 = XNode::MakePlaceholder(0, XNode::Identifier, 0x0);

            XNode *chr = new Types::String("$");
            XNode *chr2 = new Types::String("$$");

            chr->append(iden);
            chr2->append(iden2);

            dPattern->from = chr;
            //dPattern->write = chr;

            dPattern2->from = chr2;
            //dPattern2->write = chr2;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opPlaceholder", vm);
            Symbol *opRoot2 = Symbol::Reference("opPlaceholderWildcard", vm);
            XNode *propArg = XNode::MakePlaceholder(0, XNode::Identifier, 0x0),
                  *propArg2 = XNode::MakePlaceholder(0, XNode::Identifier, XNode::Wildcard);

                  std::cout << "propArg2: " << propArg2->infoString();
            opRoot->addChild(propArg);
            opRoot2->addChild(propArg2);
            dPattern->to = opRoot;
            dPattern2->to = opRoot2;
            dPattern->write = opRoot;
            dPattern2->write = opRoot2;
        };

        op->executionPriority(dPattern->priority = 1600);
        opw->executionPriority(dPattern2->priority = 1600);
        
        dPattern2->registerTo(vm);
        dPattern->registerTo(vm);
    }
};
