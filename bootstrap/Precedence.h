#ifndef CASPAR_OP_PRECED_H
#define CASPAR_OP_PRECED_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpPrecedence : public Symbol {
    public:
        OpPrecedence();
        ~OpPrecedence();

        virtual bool executeOn(XNode *node, VM *const vm) override;

        static void Bootstrap(VM *const vm);
    };
};

#endif

