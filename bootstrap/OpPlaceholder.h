#ifndef CASPAR_OPPLACEHOLDER_H
#define CASPAR_OPPLACEHOLDER_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    bool PlaceholderExec(XNode *node, VM *const vm, unsigned int flags);

    class OpPlaceholder : public Symbol {
    public:
        OpPlaceholder();
        ~OpPlaceholder();

        virtual bool executeOn(XNode *node, VM *const vm) override;

        static void Bootstrap(VM *const vm);
    };
    class OpPlaceholderWildcard : public Symbol {
    public:
        OpPlaceholderWildcard();
        ~OpPlaceholderWildcard();

        virtual bool executeOn(XNode *node, VM *const vm) override;
    };
};

#endif
