#include "Bootstrap.h"
#include "Delimiters.h"
#include "OpComma.h"
#include "OpDefine.h"
#include "OpPattern.h"
#include "OpPlaceholder.h"
#include "OpTo.h"
#include "OpComma.h"
#include "OpInclude.h"
#include "ParseRaw.h"
#include "WriteAs.h"

#include "AllTypes.h"
#include "Scope.h"
#include "VM.h"

#include "misc/Comment.h"
#include "misc/Printr.h"
#include "misc/Printf.h"
#include "PatternTo.h"
#include "misc/PatternPriority.h"
#include "misc/Priority.h"

#include "OpScope.h"
#include "OpEquals.h"
#include "OpScopeAccess.h"

#include "OpParen.h"

namespace Caspar {
    void BootstrapVM(VM *const vm) {
        OpDefine::Bootstrap(vm);
        OpPattern::Bootstrap(vm);
        OpPlaceholder::Bootstrap(vm);
        OpComma::Bootstrap(vm);
        OpInclude::Bootstrap(vm);
        OpTo::Bootstrap(vm);
        OpParen::Bootstrap(vm);

        Types::SoE *tSoE = new Types::SoE();
        Types::EoE *tEoE = new Types::EoE();
        Types::Raw *tRaw = new Types::Raw();
        Types::Property *tProperty = new Types::Property();
        Types::String *tString = new Types::String();
        Types::List *tList = new Types::List();
        Pattern *tPattern = new Pattern();

        Scope *scope = (const_cast<VM*>(vm));
        tSoE->registerTo(scope);
        tEoE->registerTo(scope);
        tRaw->registerTo(scope);
        tProperty->registerTo(scope);
        tString->registerTo(scope);
        tList->registerTo(scope);
        tPattern->Type::registerTo(scope);

        auto* patternTo = new Misc_PatternTo();
        patternTo->name("patternTo", vm);

        Misc_Comment *comment = new Misc_Comment();
        //comment->priority = 2147000;
        comment->name("comment", vm);
        auto *printr = new Misc_Printr();
        //printr->priority = 2146000;
        printr->name("printr", vm);

        auto *pattprior = new Misc_PatternPriority();
        pattprior->name("patternPriority", vm);
        
        auto *symPrior = new Misc_Priority();
        symPrior->name("priority", vm);

        VM::Immediate = new Symbol();
        VM::Immediate->name("immediate", vm);


        auto *incScope = new OpIncScope();
        incScope->name("opIncScope", vm);
        auto *decScope = new OpDecScope();
        decScope->name("opDecScope", vm);

        auto *opEquals = new OpEquals();
        opEquals->name("opEquals", vm);

        auto *opSA = new OpScopeAccess();
        opSA->name("opScopeAccess", vm);
    }
};
