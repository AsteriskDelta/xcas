#include "OpScopeAccess.h"
#include "Symbol.h"
#include "String.h"
#include "Pattern.h"
#include "VM.h"
#include "Pattern.h"
#include "String.h"
#include "Block.h"

namespace Caspar {
    OpScopeAccess::OpScopeAccess() : Symbol() {

            this->type = XNode::Identifier;
    }
    OpScopeAccess::~OpScopeAccess() {

    }

    bool OpScopeAccess::executeOn(XNode *node, VM *const vm) {
        node->resolve(vm);
        XNode *src = node->firstChild, *sub = node->firstChild->next;

        src->resolve(vm);
        if(src->symbol() == nullptr) {
            std::cerr << "OpScopeAccess got a src xnode with a null symbol!\n";
            return false;
        }

        class Block *scp = src->symbol()->as<class Block>();
        if(scp == nullptr) {
            std::cerr << "OpScopeAccess got a src node that wasn't a Block object!\n";
            return false;
        }

        bool setScope = false;
        if(vm->scope != scp) setScope = true;

        if(setScope) vm->pushScope(scp);
        sub->resolve(vm);
        if(setScope) vm->popScope();

        node->replaceWith(sub);
        delete node;

        return true;
    }

};
