#ifndef CASPAR_OPSCOPEACCESS_H
#define CASPAR_OPSCOPEACCESS_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpScopeAccess : public Symbol {
    public:
        OpScopeAccess();
        ~OpScopeAccess();

        virtual bool executeOn(XNode *node, VM *const vm) override;
    };
};

#endif
