#include "OpDefine.h"
#include "Symbol.h"
#include "String.h"
#include "VM.h"
#include "Pattern.h"
#include "type/List.h"

namespace Caspar {
    OpDefine::OpDefine() : Symbol() {
        this->type = XNode::Identifier;
    }
    OpDefine::~OpDefine() {

    }

    bool OpDefine::executeOn(XNode *node, VM *const vm) {
        //node->resolve(vm);
        std::string name = node->children()[0].str();
        //Symbol *sym = node->children()[0].symbol();
        Symbol *sym = vm->scope->Scope::resolve(name);
        std::cout << "!!!OpDefine got " << name << " = " << sym << /*sym->infoString() */""<< "\n";

        if(sym == nullptr) {
            sym = new Symbol();
            sym->type = node->children()[0].type;
            sym->name(name, vm->scope);
        }

        for(auto child = node->firstChild->next->firstChild; child != nullptr; child = child->next) {
            if(child->typeName() == "list") {
                Types::List *listArg = dynamic_cast<Types::List*>(child);
                for(auto it = listArg->begin(); it; ++it) sym->addProperty(it);
            } else {
                //std::cout << "define got arg " << child->infoString() << " @ " << child << ", sym = " << sym << "\n";
                sym->addProperty(child);
            }
        }

        return true;
    }

    void OpDefine::Bootstrap(VM *const vm) {
        OpDefine *op = new OpDefine();
        class Pattern *dPattern = new class Pattern();
        op->name("opDefine", vm);

        {//The precedent
            XNode *iden = XNode::MakePlaceholder(0, XNode::Identifier, XNode::Wildcard);//new XNode();
            //iden->setFlag(XNode::PlaceHolder | XNode::Wildcard);
            //iden->setType(XNode::Identifier);

            XNode *chr = new Types::String(":");

            XNode *props = XNode::MakePlaceholder(1, XNode::Expression, XNode::Wildcard);//new XNode();
            //props->setFlag(XNode::PlaceHolder | XNode::Wildcard);
            //props->setType(XNode::Expression);


            iden->append(chr);
            chr->append(props);

            dPattern->from = iden;
            dPattern->write =iden;
        };

        {//The antecedent
            Symbol *opRoot = Symbol::Reference("opDefine", vm);
            XNode *propArg = XNode::MakePlaceholder(0, XNode::Identifier, XNode::Wildcard),
                  *exprContainer = XNode::MakeContainer(),
                  *exprArg = XNode::MakePlaceholder(1, XNode::Expression, XNode::Wildcard);

            exprContainer->addChild(exprArg);

            opRoot->addChild(propArg);
            opRoot->addChild(exprContainer);
            dPattern->to = opRoot;
        };

        dPattern->priority = 999;
        op->executionPriority(-10000);

        /*{//And finally, output
            XNode *iden = XNode::MakePlaceholder(0, XNode::Identifier, XNode::Wildcard);//new XNode();
            XNode *chr = new String(":");
            XNode *props = XNode::MakePlaceholder(1, XNode::Expression, XNode::Wildcard);//new XNode();

            iden->append(chr);
            chr->append(props);

            dPattern->setFrom(iden);
        };*/

        dPattern->registerTo(vm);
    }
};
