#ifndef CASPAR_OPPATTERN_H
#define CASPAR_OPPATTERN_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpPattern : public Symbol {
    public:
        OpPattern();
        ~OpPattern();

        virtual bool executeOn(XNode *node, VM *const vm) override;
        static void Bootstrap(VM *const vm);
    };
};

#endif
