#ifndef CASPAR_OPINCLUDE_H
#define CASPAR_OPINCLUDE_H
#include "iCaspar.h"
#include "Symbol.h"

namespace Caspar {
    class OpInclude : public Symbol {
    public:
        OpInclude();
        ~OpInclude();

        virtual bool executeOn(XNode *node, VM *const vm) override;
        static void Bootstrap(VM *const vm);
    };

    class OpIncludeStd : public Symbol {
    public:
        OpIncludeStd();
        ~OpIncludeStd();

        virtual bool executeOn(XNode *node, VM *const vm) override;
        static void Bootstrap(VM *const vm);
    };
};

#endif
