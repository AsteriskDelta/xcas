#include "Patterns.h"
#include <fstream>

#include <algorithm> 
#include <cctype>
#include <locale>

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

namespace Caspar {
    namespace Patterns {
        std::vector<Pattern*> All;
        std::vector<std::string> AllPatternFiles;
        
        static bool AllLoaded = false;
        
        void FindAllFiles() {
            AllPatternFiles.push_back("patterns01.txt");
        }
        
        bool LoadAll() {
            FindAllFiles();
            
            AllLoaded = true;
            
            for(const std::string& file : AllPatternFiles) {
                bool fLoaded = LoadFrom(file);
                if(!fLoaded) {
                    std::cerr << "WARNING: Could not load pattern file at \"" << file << "\"\n";
                }
                
                //Always pass for now
                //AllLoaded &= fLoaded;
            }
            
            return AllLoaded;
        }
        
        bool Loaded() {
            return AllLoaded;
        }
        
        
        bool LoadFrom(const std::string& file) {
            std::fstream fs(file, std::fstream::in);
            if(!fs) return false;
            
            std::string line; unsigned int lineNumber = 0;
            while(fs.good() && std::getline(fs, line) ) {
                lineNumber++;
                if(line.size() < 2 || line[0] == '@') continue;
                
                std::string groupIden = "", aliasIden = "", callIden = "";
                
                if(line[0] == ';' && line[1] == ';') {
                    //std::cout << "BEGIN lsearch: " << line << "\n";
                    line = line.substr(2, line.size() - 2);
                    std::size_t delimPos = line.find(":");
                    //std::cout << "AFTER lsearch: " << line << "\n";
                    if(delimPos == std::string::npos) {
                        FileError("No pattern group delimiter found (colon)", file, lineNumber, line);
                        return false;
                    }
                    
                    groupIden = line.substr(0, delimPos);
                    line = line.substr(delimPos+1, line.size() - delimPos - 1);
                } else if(line[0] == ';') {
                    line = line.substr(1, line.size() - 1);
                    std::size_t delimPos = line.find(":");
                    if(delimPos == std::string::npos) {
                        FileError("No pattern alias delimiter found (colon)", file, lineNumber, line);
                        return false;
                    }
                    
                    aliasIden = line.substr(0, delimPos);
                    line = line.substr(delimPos + 1, line.size() - delimPos - 1);
                }
                
                std::size_t callSep = line.find("\\");
                if(callSep != std::string::npos) {
                    callIden = line.substr(callSep+1, line.size() - callSep - 1);
                    line = line.substr(0, callSep);
                } else if(aliasIden.empty()) {
                    FileError("No call function name found (after the first backslash)", file, lineNumber, line);
                    return false;
                }
                
                std::cout << "PARSE " << line << "\t\tcalls -> " << callIden << "\n\t";
                
                //Now, parse out symbol types and isolate the operators themselves
                std::size_t symPos = 0;
                while(symPos < line.size()) {
                    std::size_t newSymPos = line.find("$", symPos);
                    if(newSymPos - symPos > 0){// || (newSymPos == std::string::npos && symPos < line.size())) {
                        std::string literal = line.substr(symPos, newSymPos - symPos);
                        trim(literal);
                        //std::cout << "\t\tliteral '" << literal << "', a string of size " << literal.size() << " from s=" << symPos << ", e=" << (newSymPos) << "\n";
                        if(!literal.empty()) {
                            std::cout << "\"" << literal << "\"";
                        }
                    }
                    
                    std::size_t oldSymPos = symPos;
                    symPos = newSymPos;
                    if(symPos == std::string::npos || symPos >= line.size()) break;
                    
                    unsigned int off;
                    for(off = 0; off < 128 && symPos+off < line.size(); off++) {
                        char c = line[symPos+off];
                        //Because c++'s isalnum matches some special chars...
                        bool good = (c == '$' || (c >= 48 && c <= 57) || (c >= 65 && c <= 122)) && (c < 91 || c > 96);// || isalpha(c);
                        //std::cout << "\t\t\t" << c << "\t" << good << "\n";
                        if(!good) break;
                    }
                    
                    std::size_t typeLen = off;// - (oldSymPos == 0? 0 : 1);
                    //if(1 + typeLen + symPos >= line.size()) typeLen = line.size() - symPos - 2;
                    
                    std::string typeIden = line.substr(symPos+1, typeLen - 1);
                    std::cout << " $'" << typeIden << "'";
                    
                    //std::cout << "\t\ttypeIden is '" << typeIden << "', a string of size " << typeIden.size() << " from s=" << symPos << ", e=" << (symPos + typeLen) << "\n";
                    
                    symPos += typeLen;//(oldSymPos == 0? 1 : 0);//(typeLen + symPos + 1 >= line.size()? 0 : 0);
                }
                
                std::cout << "\n";
            }
            
            return true;
        }
        
    };
};
