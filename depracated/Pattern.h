#include "iCaspar.h"
#include <vector>

namespace Caspar {
    class Pattern {
    public:
        Pattern();
        
        struct Segment {
            bool literal, optional = false;
            std::string raw;
            
            //Resolved/cached data
            
        };
        std::vector<Segment> segments;
        
        //void* callsFunction;
        
        
    protected:
        
    };
};
