#include "iCaspar.h"
#include <vector>

namespace Caspar {
    namespace Patterns {
        bool LoadFrom(const std::string& file);
        bool LoadAll();
        
        bool Loaded();
        
        extern std::vector<Pattern*> All;
    };
};
